//
//  redditGroup.swift
//  WelbornEdwardCE04
//
//  Created by Roy Welborn on 6/13/20.
//  Copyright © 2020 Roy Welborn. All rights reserved.
//

import Foundation
import UIKit

class redditGroup
{
    /* Stored Properties */
    let subReddit: String
    let title: String?
    let authorName : String?
    var thumbnail: UIImage?
    
    /*Initializers*/
    init(subReddit: String, title: String, authorName: String, imageURLString: String)
    {
        self.subReddit = subReddit
        self.title = title
        self.authorName = authorName
        
        // Changed due to feedback from previous course
        if imageURLString.contains("http"),
             let url = URL(string: imageURLString),
             var urlComp = URLComponents(url: url, resolvingAgainstBaseURL: false)
         {
             // Change to secure server. All thumbs seem to come from "redditmedia" servers.
             // A simple borswer test proved that redditmedia servers allow secure access.
             // Using URLComponent object to quickly alter the incomming URL from http to https
             // Alternatives include whitelisting a domain with ATS if no secure version is available
             // Or disabling ATS altogether which is HIGHLY undesireable.
             urlComp.scheme = "https"

             if let secureURL = urlComp.url {

                 let imageData = try? Data.init(contentsOf: secureURL)
                 self.thumbnail = UIImage(data: imageData!)

             }
        }
    }
}
/* Feedback from previous course */
/*
 Accepted 2 days late.

 JSON Remote Data: Excellent 15/15
 All requirements met.
 JSON Parsing: Fair 11.25/25
 The logic necessary to exclude posts that do not contain a thumbnail is not present. The best way to approach this is to check to see if the data in "thumbnail" contains a URL by checking for the presence of a scheme (http || https), if there is a scheme then append to the array of objects. It would also be a better approach to parse the individual properties within a guard statement and without the use of the nil coalescing operator, since for example if there isn't a title, then we really don't want the post.
 Model Objects: Excellent 20/20
 All requirements met.
 UITableView: Excellent 20/20
 It is not necessary to use the .description when assigning properties to the UI. We use .description when we want to get the String value of a number since these are already strings, the use of .description is redundant.
 Code Structure & Efficiency: Excellent 20/20
 All requirements met.
 Roy, overall this is very solid work, please see the section specific comments above and let me know if you have any questions.*/
