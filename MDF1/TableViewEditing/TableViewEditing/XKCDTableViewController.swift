//
//  XKCDTableViewController.swift
//  TableViewEditing


import UIKit

class XKCDTableViewController: UITableViewController {


    //For tracking ongoing tasks
    var numTasks = 0

    //Data Array
    var comics = [Comic]()

    //Endpoint Manipulation Variables
    let urlScheme = "https:"
    let urlHost = "//xkcd.com/"
    let urlPathEnd = "/info.0.json"

    //Range of Comics to get. API only returns 1 at a time.
    var firstComic = 303
    var lastComic = 335

    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()


        //Loop through comics. Pull and Map to comics Array
        for i in firstComic...lastComic {
            parseJsonAt(dataUrl: urlScheme + urlHost + i.description + urlPathEnd )
        }


    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return comics.count
    }


    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell_ID1", for: indexPath)  as! XKCDTableCell

        // Configure the cell...
        cell.titleLabel.text  = comics[indexPath.row].title
        cell.comicImage.image = comics[indexPath.row].img
        cell.numberLabel.text = comics[indexPath.row].comicNum.description

        return cell
    }


    /*
     // Override to support conditional editing of the table view.
     override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the specified item to be editable.
     return true
     }
     */

    /*
     // Override to support editing the table view.
     override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
     if editingStyle == .delete {
     // Delete the row from the data source
     tableView.deleteRows(at: [indexPath], with: .fade)
     } else if editingStyle == .insert {
     // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
     }
     }
     */

    /*
     // Override to support rearranging the table view.
     override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

     }
     */

    /*
     // Override to support conditional rearranging of the table view.
     override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the item to be re-orderable.
     return true
     }
     */

    /*
     // MARK: - Navigation

     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */

    //MARK: - JSON Parsing
    func parseJsonAt(dataUrl: String) {

        //Create Session and bind URL to non optional
        let session = URLSession.shared

        guard let validURL = URL(string: dataUrl)  else { return }

        //Increment tasks as they begin
        numTasks += 1
        let task = session.dataTask(with: validURL, completionHandler: { (opt_data, opt_response, opt_error) in

            //Bail Out on error
            if opt_error != nil { assertionFailure(); return }

            //Check the response, statusCode, and data
            guard let response = opt_response as? HTTPURLResponse,
                response.statusCode == 200,
                let data = opt_data
                else { assertionFailure(); return
            }

            do {
                //De-Serialize data object
                guard let mainObj = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any],
                    let month = mainObj["month"] as? String,
                    let num = mainObj["num"] as? Int,
                    let year = mainObj["year"] as? String,
                    let altText = mainObj["alt"] as? String,
                    let imgSource = mainObj["img"] as? String,
                    let title = mainObj["title"] as? String,
                    let day = mainObj["day"] as? String
                    else {
                        assertionFailure()
                        return
                }

                //Map Object
                self.comics.append(Comic(month: month, num: num, year: year, altText: altText, imgSource: imgSource, title: title, day: day))

            }
            catch {
                print(error.localizedDescription)
                assertionFailure();
            }

            //decrement tasks as they finish
            self.numTasks -= 1
            if self.numTasks == 0 {
                DispatchQueue.main.async {

                    //Do Stuff
                    self.comics.sort(by: { (c1, c2) -> Bool in
                        c2.comicNum > c1.comicNum
                    })
                    self.tableView.reloadData()
                    print("TableView was Reloaded")
                }
            }
        })
        task.resume()
    }

}
