//
//  Comic.swift
//  TableViewEditing


import Foundation
import UIKit


class Comic {

    let month: Int
    let comicNum: Int
    let year: Int
    let altText: String
    let img: UIImage
    let title: String
    let day: Int

    init(month:String, num:Int, year:String, altText:String, imgSource:String, title:String, day:String) {

        self.month =  month.IntValue()
        self.comicNum = num
        self.year = year.IntValue()
        self.altText = altText
        self.img = imgSource.ImageValue(makeSecure: true)
        self.title = title
        self.day = day.IntValue()
    }
}


