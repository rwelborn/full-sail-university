
//
//  String_EXT.swift
//  TableViewEditing

import Foundation
import UIKit


extension String {

    //Returns an Int value from a given String If possible. -1 indicates failure
    func IntValue() -> Int {

        if let i = Int(self) {
            return i
        }
        //Cast Failed
        return -1
    }

    //Returns a UIImage downloaded from a urlString if possible.
    func ImageValue(makeSecure: Bool) -> UIImage {

        var url: URL? = nil

        do {
            if makeSecure == true {
                if var comp = URLComponents(string: self) {
                    comp.scheme = "https"
                    url = comp.url!
                }
            }
            else {
                url = URL(string: self)!
            }

            //Try to create the image from the string url. Forcing URL.
            if let image = UIImage(data: try Data(contentsOf: url!)) {
                return image
            }
        }
        catch {
            print(error.localizedDescription)
        }

        //We failed set default image.
        return #imageLiteral(resourceName: "DefaultXKCD")
    }
}
