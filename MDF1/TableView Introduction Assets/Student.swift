//
//  Student.swift
//  TableViewBasics
//


import Foundation
import UIKit

class Student {

    /* Stored Properties */
    var name: String
    var university: String
    var avatarURL: URL!


    /* Computed Properties */


    /* Initializers */
    init(name: String, university: String, avatarString: String) {

        self.name = name
        self.university = university
        self.avatarURL = URL(string: avatarString)
    }

    /* Methods */
}
