//
//  VC_Extension.swift
//

import Foundation
import UIKit

extension ViewController {

    //Helper method to download and parse json at a url(String)
    func downloadAndParse(jsonAtUrl urlString: String) {

        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        if let validURL = URL(string: urlString) {

            let task = session.dataTask(with: validURL, completionHandler: { (opt_data, opt_response, opt_error) in

                //Bail Out on error
                if opt_error != nil { return }

                //Check the response, statusCode, and data
                guard let response = opt_response as? HTTPURLResponse,
                    response.statusCode == 200,
                    let data = opt_data
                    else { return }

                do {
                    //De-Serialize data object
                    if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [Any] {
                        //Parse Data
                        for firstLevelItem in json {
                            guard let student = firstLevelItem as? [String: Any],
                                let name = student["name"] as? String,
                                let university = student["university"] as? String,
                                let avatarURL = student["avatar"] as? String
                                else { continue }
                            //Map to Model Objects
                            
                        }
                    }
                }
                catch {
                    print(error.localizedDescription)
                }
            })
            task.resume()
        }
    }
}
