//ViewController.swift
//RemoteDataLecture_StarterFiles
//
//Copyright © 2019 Patti Dacosta. All rights reserved.

import UIKit

class ViewController: UIViewController {
    
   
    @IBOutlet weak var labelCustomerNumber: UILabel!
    @IBOutlet weak var labelNumOrders: UILabel!
    @IBOutlet weak var labelTotalPurch: UILabel!
    @IBOutlet weak var textFieldNote: UITextField!
    
    
    
    
    

    var currentCustomerIndex = 0
    
    var customers = [Customer]() //Empty array of Customer objects
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        /* Retrieve JSON data from a remote course */
        /* Create a default configuration */
        let config = URLSessionConfiguration.default
        
        /* Create a session */
        let session = URLSession(configuration: config)
        // Validate URL to make sure it is not a broken link
        if let validURL = URL(string: "https://fullsailmobile.firebaseio.com/Q4.json") {
            // Create a task that will download whatever is in the validURL as a data object
            let task = session.dataTask(with: validURL) { (data, response, error) in
                // If there is an error we are going to bail our of this entire method
                if let error = error {
                    print("Data task failed with error: " + error.localizedDescription)
                    return
                }
                // If we get here that means that we have recieved the info at the URL as a data object and we can now use it
                print("success")
                
                // Check the response status, validate the data
                guard let httpResponse = response as? HTTPURLResponse,
                    httpResponse.statusCode == 200,
                    let validData = data
                else {
                    print("JSON object creation failed")
                    return
                }
                do {
                    let jsonObj = try JSONSerialization.jsonObject(with: validData, options: .mutableContainers) as? [Any]
                    
                    // Call the parse method
                    self.Parse(jsonObject: jsonObj)
                }
                catch {
                    print(error.localizedDescription)
                }
            }
            task.resume()
        }

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func Parse(jsonObject: [Any]? ) {
        
        guard let json = jsonObject
            else { print("Parse Failed to Unwrap jsonObject."); return }
        
        //Loop
        for firstLevelItem in json {
            //Try to convert
            guard let object = firstLevelItem as? [String: Any],
                let fname = object["first_name"] as? String,
                let lname = object["last_name"] as? String,
                let custNumber = object["customer_number"] as? Int,
                //purchase is just another object so we do the same thing
                let purchase = object["purchase"] as? [String: Any],
                //Will have to convert to "Date" object
                let time = purchase["time"] as? String,
                let date = purchase["date"] as? String,
                //Will have to Trim $
                let amount =  purchase["amount"] as? String
                //if not then continue to next iteration
                else {
                    continue
            }
            
            func addTransaction(c: Customer) {
                
                
                let cardNumber = purchase["card_number"] as? String
                c.transactions.append(
                    Transaction(firstname: fname, lastName: lname, time: time, date: date, amount: amount, cardNumber: cardNumber)
                )
            }
            
            
            
            
            //TODO: Build an Object here
            
            //Check to see if this current customer number exists yet.
            //Filter is a fast, elegant way to make this comparison.
            //Looping through each object and checking would be ugly and slow.
            let filteredCustomers = customers.filter({ (customer) -> Bool in
                return customer.customerNumber == custNumber
            })
            
            //If Filtered Array.count is 0, then that customer doesn't exist yet.
            //We should instantiate a new customer object and append to our array.
            if filteredCustomers.count == 0 {
                //Instantiate a new Customer Object and append to array
                customers.append(Customer(customerNumber: custNumber))
                //Create Transaction
                addTransaction(c: customers.last!)
            }
                
                //If filtered Array.count is 1, We already have a customer object for this number.
                //In this case we want to modify that customer object instead of creating a new one.
            else if filteredCustomers.count == 1 {
                /* See Note: Reference vs. Value types */
                filteredCustomers[0].custNote = "This customer has been modified."
                //Create transaction
                addTransaction(c: filteredCustomers[0])
            }
                
                //This should never happen.
                /* See Note: Assertions */
                //Assertion Failure so that as we're building if this ever happens we know we've screwed up.
            else {
                assertionFailure("Something bad happened. No customers should exist more than once.")
            }
        }
        displayCurrentCustomerIndex()
        
    }
    
    
    func displayCurrentCustomerIndex() {
        
        
        DispatchQueue.main.async {
        
            self.labelCustomerNumber.text =  self.customers[self.currentCustomerIndex].customerNumber.description
        
            self.labelNumOrders.text = self.customers[self.currentCustomerIndex].numTransactions.description
        
            self.labelTotalPurch.text = "$" + self.customers[self.currentCustomerIndex].totalOfAllTransactions.description
        
            if let note = self.customers[self.currentCustomerIndex].custNote {
                self.textFieldNote.text = note
            }
            else {
                self.textFieldNote.text = "No Notes"
            }
        }
        
    }
    
    
    
    @IBAction func CurrentCustomerChanged(_ sender: UIButton) {
        
        currentCustomerIndex += sender.tag
        
        if currentCustomerIndex < 0 {
            currentCustomerIndex = customers.count - 1
        }
        else if currentCustomerIndex >= customers.count {
            currentCustomerIndex = 0
        }
        
        displayCurrentCustomerIndex()
    }
    
    


}

