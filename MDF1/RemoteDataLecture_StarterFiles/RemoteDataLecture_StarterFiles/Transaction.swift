//Transaction.swift
//RemoteDataLecture_StarterFiles
//
//Copyright © 2019 Patti Dacosta. All rights reserved.

import Foundation

class Transaction {
    
    /* Stored Properties */
    let personnelName: String
    let dateTime: Date
    let cardNumber: Int! //will sometimes not exist
    let amount: Double
    
    /* Computed Properties */
    
    
    /* Initializers */
    init(personnelName: String, dateTime: Date, cardNumber: Int!, amount: Double ) {
        self.personnelName = personnelName
        self.dateTime = dateTime
        self.cardNumber = cardNumber
        self.amount = amount
    }
    
    convenience init (firstname: String, lastName: String, time: String, date: String, amount: String, cardNumber: String? = nil) {
        
        //Combine first and Last Name
        let name = firstname + " " + lastName
        
        //Format date and time into a Date Object
        let dateformatter =  DateFormatter()
        dateformatter.dateFormat = "MM-dd-yyyy HH:mm"
        let dt = dateformatter.date(from: date + " " + time)
        
        //Trim $ from amount and cast to Double
        let amt = Double(amount.trimmingCharacters(in: CharacterSet.init(charactersIn: "$")))
        
        //Check for cardnumber
        if let cardNumber = cardNumber, let cn = Int(cardNumber) {
            self.init(personnelName: name, dateTime: dt!, cardNumber: cn, amount: amt! )
        }
        else {
            self.init(personnelName: name, dateTime: dt!, cardNumber: nil, amount: amt! )
        }
        
    }
}
