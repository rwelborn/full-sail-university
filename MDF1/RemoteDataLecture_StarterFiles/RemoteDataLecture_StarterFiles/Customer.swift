//Customer.swift
//RemoteDataLecture_StarterFiles
//
//Copyright © 2019 Patti Dacosta. All rights reserved.

import Foundation

class Customer {
    
    /* Stored Properties */
    let customerNumber: Int
    private var customerNote: String! //may not always have a value
    var transactions: [Transaction] //Array of Transaction Objects
    
    /* Computed Properties */
    var custNote: String? {
        get {
            return self.customerNote
        }
        set {
            self.customerNote = newValue
        }
    }
    var numTransactions: Int {
        return transactions.count
    }
    
    var totalOfAllTransactions: Double {
        var total = 0.0
        for t in transactions {
            total += t.amount
        }
        return total
    }
    
    
    /* Initializers */
    init(customerNumber: Int, customerNote: String! = nil, transactions: [Transaction] = [Transaction]() ) {
        self.customerNumber = customerNumber
        self.customerNote = customerNote
        self.transactions = transactions
    }
}
