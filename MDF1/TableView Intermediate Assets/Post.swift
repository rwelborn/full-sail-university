//
//  Post.swift
//  TableViewIntermediate

import Foundation
import UIKit

enum ParsingError: Error {
    case PostParsing
    case Generic
}

//Model Object Representing a Reddit Post
class Post {

    /* Stored Properites */
    let title: String
    let author: String
    var thumbImage: UIImage! = nil
    let numComments: Int
    let score:Int
    let date: Date
    let postText: String
    let subReddit: String


    /* Computed Properties */


    /* Initializers */
    init(jsonRedditPost post: [String: Any]) throws {

        //Parse the Post.
        guard
            let title   = post["title"] as? String,
            let author  = post["author"] as? String,
            let thumbString = post["thumbnail"] as? String,
            let numComments = post["num_comments"] as? Int,
            let score = post["score"] as? Int,
            let date = post["created_utc"] as? Int,
            let postText = post["selftext"] as? String,
            let subReddit = post["subreddit"] as? String
            else {
                //Throw custom error is parsing fails. We can catch it and determine what to do at the init callsite.
                throw ParsingError.PostParsing
        }

        self.title = title
        self.author = author
        self.numComments = numComments
        self.score = score
        self.postText = postText
        self.subReddit = subReddit
        //Date is provided by JSON as a seconds since Jan 1st 1970.
        //DateTime objects in most platforms use this paradigm. TimeInterval can be created directly with that value.
        //In turn a Date can be created with a TimeInterval
        self.date = Date(timeIntervalSince1970: TimeInterval(date))

        /* If there is no thumbnail the field will be set to "self", "default", or possibly other non-url strings. Checking against "http" will allow us to tell for sure no matter what the non-image string may be. */
        if thumbString.contains("http"),
            let url = URL(string: thumbString),
            var urlComp = URLComponents(url: url, resolvingAgainstBaseURL: false)
        {
            // Change to secure server. All thumbs seem to come from "redditmedia" servers.
            // A simple borswer test proved that redditmedia servers allow secure access.
            // Using URLComponent object to quickly alter the incomming URL from http to https
            // Alternatives include whitelisting a domain with ATS if no secure version is available
            // Or disabling ATS altogether which is HIGHLY undesireable.
            urlComp.scheme = "https"

            if let secureURL = urlComp.url {

                let imageData = try Data.init(contentsOf: secureURL)
                self.thumbImage = UIImage(data: imageData)

            }
        }
    }

    
    /* Methods */
    
    
}
