//
//  ZipCodeClass.swift
//  WelbornEdwardCE06
//
//  Created by Roy Welborn on 6/20/20.
//  Copyright © 2020 Roy Welborn. All rights reserved.
//

import Foundation

class ZipCode
{
    /* Stored Variables */
    let city: String
    let population: Int
    let state: String
    let zipCode: String
    
    /* Initializers */
    init(city: String, population: Int, state: String, zipCode: String)
    {
        self.city = city
        self.population = population
        self.state = state
        self.zipCode = zipCode
    }
}
