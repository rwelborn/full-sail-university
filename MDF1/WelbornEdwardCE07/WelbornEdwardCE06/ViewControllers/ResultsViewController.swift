//
//  ViewController.swift
//  WelbornEdwardCE06
//
//  Created by Roy Welborn on 6/20/20.
//  Copyright © 2020 Roy Welborn. All rights reserved.
//

import UIKit

/* Global Private Declaration */
private let g_CellID01 = "CellID01"

class ResultsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, SearchViewControllerDelegate {
    
    /* Variable Declarations */
    var returnedData = [ZipCode]()
    
    
    /* button Declarations */
    @IBAction func clearButton(_ sender: Any) {
        returnedData.removeAll()
        self.title = "Press Hourglass to Start"
        tableView.reloadData()
    }
    @IBOutlet weak var clearButton: UIBarButtonItem!
    
    /* TableView Declaration */
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Press Hourglass to Start"
         tableView.rowHeight = 60
    }
    
    func numberOfSections(in tableView: UITableView) -> Int
     {
         // #warning Incomplete implementation, return the number of sections
         return 1
     }
      func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
           return returnedData.count
       }
       
       func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: g_CellID01, for: indexPath)  as? ResultsTableViewCell

         // Configure the cell...
         cell?.cityLabel?.text = returnedData[indexPath.row].city + ", " + returnedData[indexPath.row].state
         cell?.populationLabel?.text = "Population: \(returnedData[indexPath.row].population.description)"
         cell?.zipLabel?.text = "Zip: \(returnedData[indexPath.row].zipCode)"
        return cell ?? ResultsTableViewCell()
       }
       
    func passTheData(data: [ZipCode]) {
        print(data)
        returnedData = data
        tableView.reloadData()
        self.title = "Results: \(returnedData.count) records"
    }
        // MARK: - Navigation
    
        // In a storyboard-based application, you will often want to do a little preparation before navigation
        override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
            // Get the new view controller using segue.destination.
            // Pass the selected object to the new view controller.
                
            guard let searchViewController = segue.destination as? SearchViewController else { return }
            
            searchViewController.delegate = self
            
        }
    }

/* MARK: Feedback from previous Attempt */
/*
 Excellent!

 JSON: Excellent 10/10
 All requirements met.
 Results View: Excellent 20/20
 All requirements met.
 Search View: Excellent 30/30
 All requirements met.
 Model Objects: Excellent 20/20
 All requirements met.
 Code Structure & Efficiency: Excellent 20/20
 All requirements met.
 Roy, solid work on the SearchBar lab! This work is clean and concise, well-commented, and meets all of the rubric requirements earning a top grade. Nice job!
 
 */




