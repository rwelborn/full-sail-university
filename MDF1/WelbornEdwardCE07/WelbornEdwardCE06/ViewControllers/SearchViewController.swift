//
//  SearchViewController.swift
//  WelbornEdwardCE06
//
//  Created by Roy Welborn on 6/20/20.
//  Copyright © 2020 Roy Welborn. All rights reserved.
//

import UIKit

/* Global Private Declaration */
private let g_CellID01 = "CellID01"

class SearchViewController: UIViewController, UISearchResultsUpdating, UISearchBarDelegate, UITableViewDelegate, UITableViewDataSource
{
    /* Variable Declarations */
    var delegate: SearchViewControllerDelegate?
    var zipCodeData = [ZipCode]()
    var filteredData = [ZipCode]()
    
    /* Search Controller Declaration */
    /* Passing nil tells our search controller to use the current tableviewcontroller to display the results */
    var searchController = UISearchController(searchResultsController: nil)
    
    /* TableView Declaration */
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.rowHeight = 60
        
        // Do any additional setup after loading the view.
        if let path = Bundle.main.path(forResource: "zips", ofType: ".json")
        {
            // create the url with the path
            let url = URL(fileURLWithPath: path)
            
            do
            {
                // create a data object from the file url
                let data = try Data.init(contentsOf: url)
                // create a json object from the binary data file
                let jsonObj = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [Any]
                // parse through json object and start instantiating the carinfo object
                //MARK: Broken Here, it is reading in to the jsonObj
                Parse(jsonObject: jsonObj)
            }
            catch
            {
                print(error.localizedDescription)
            }
        }
    }
    
    // Parse the json data into employeeData
    func Parse(jsonObject: [Any]?)
    {
        // Safely Bind jsonObject to the non-optional json
        
        guard let jsonObj = jsonObject
            else
        {
            print("Parsing Failed in Employee Data File")
            return
        }
        // Loop Through First Level Items
        // print(jsonObj)
        for items in jsonObj
        {
            // print(items)
            // Try to convert 1st level objects in a [String: Any]
            guard let object = items as? [String: Any],
                let cityData = object["city"] as? String,
                let stateData = object["state"] as? String,
                let populationData = object["pop"] as? Int,
                let zipCode = object["_id"] as? String
                
                // if anything in guard fails, continue to next
                else { continue }
            
            // map the skills data array from the object
            let zipCodeInfo = ZipCode(city: cityData, population: populationData, state: stateData, zipCode: zipCode)
            // print(zipCodeInfo.city + ", " + zipCodeInfo.state)
            //  print(past_EmployersData.count)
            zipCodeData.append(zipCodeInfo)
            // print(returnedData.count)
        }
        filteredData = zipCodeData
        /* Add search bar as a naviation item */
        navigationItem.hidesSearchBarWhenScrolling = false
        navigationItem.searchController = searchController
        navigationItem.title = "Search Page"
        /* Setup Search Controller */
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.definesPresentationContext = true
        
        /* To recieve update to searches here in this tableviewcontroller */
        searchController.searchResultsUpdater = self
        
        /* Setup search bar of search controller */
        searchController.searchBar.scopeButtonTitles = ["All", "MO", "KS"]
        searchController.searchBar.delegate = self
        
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        /* Pass the data and close the searchViewController */
        delegate?.passTheData(data: filteredData)
        self.navigationController?.popViewController(animated: true)
        
    }
    
    // delegate function to pass the data to the firstviewcontroller
    func passTheData(data: [ZipCode]) {
        filteredData = data
    }
    
    // Mark: TableView Datasource
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        // #warning Incomplete implementation, return the number of rows
        return filteredData.count
    }
    
    /* Mark: TableView Delegate */
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: g_CellID01, for: indexPath) as? SearchTableViewCell
        
        // Configure the cell...
        cell?.cityLabel?.text = filteredData[indexPath.row].city + ", " + filteredData[indexPath.row].state
        cell?.populationLabel?.text = "Population: \(filteredData[indexPath.row].population.description)"
        cell?.zipLabel?.text = "Zip: \(filteredData[indexPath.row].zipCode)"
        return cell ?? SearchTableViewCell()
    }
    
    /* Mark: -- Search Callbacks */
    func updateSearchResults(for searchController: UISearchController)
    {
        
        // using the incoming search controller, gather current searchbar text and scope title selected, then filter the results. Needs a sepaerate array to hold the filtered information
        // Get the text the user wants to search for
        let searchText = searchController.searchBar.text
        
        // get the scope button title that the user has selected
        let selectedScope = searchController.searchBar.selectedScopeButtonIndex
        let allScopeTitles = searchController.searchBar.scopeButtonTitles!
        let scopeTitle = allScopeTitles[selectedScope]
        
        // Filter the Data
        // dump full dataset into the arrary we will use to filtering
        filteredData = zipCodeData
        
        // If the user entered anything into the search field then filter based on that entry
        if searchText != ""
        {
            /* Below is an inline and un-named fucionted passed into the .filter call as a paramter .
             // .filter is method that takes in another method as a parameter.  Programming
             // 'contact' is the name we've given to our array element that is currently bgeing filtered.
             // Each element in the array will return true if it passes the criteria and will then be
             // included in the filteredArray.
             // Each element in the filteredArray will be then shown in our TableView
             */
            
            
            filteredData = filteredData.filter(
                {
                    (citySearch) -> Bool in
                    return citySearch.city.lowercased().range(of: searchText!.lowercased()) != nil
            })
        }
        // filter again based on scope
        if scopeTitle != "All"
        {
            
            filteredData = filteredData.filter(
                {
                    $0.state.range(of: scopeTitle) != nil
            })
        }
        tableView.reloadData()
    }
    /* This delegate method is called when the user changes the scope.  Since we handle the in our
     // impemtation of the "updatesearchresults" we can just call that method and let it take
     // care of everything
     */
    func searchBar(_ searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int) {
        updateSearchResults(for: searchController)
    }
}
