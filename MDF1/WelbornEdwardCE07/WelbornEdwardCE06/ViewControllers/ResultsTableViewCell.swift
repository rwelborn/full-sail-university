//
//  ResultsTableViewCell.swift
//  WelbornEdwardCE06
//
//  Created by Roy Welborn on 6/21/20.
//  Copyright © 2020 Roy Welborn. All rights reserved.
//

import Foundation
import UIKit

 class ResultsTableViewCell : UITableViewCell
{
    /* Label Declarations */
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var populationLabel: UILabel!
    @IBOutlet weak var zipLabel: UILabel!
    
}
