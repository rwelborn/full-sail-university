//
//  SearchViewControllerDelegate.swift
//  WelbornEdwardCE06
//
//  Created by Roy Welborn on 6/20/20.
//  Copyright © 2020 Roy Welborn. All rights reserved.
//

import Foundation

protocol SearchViewControllerDelegate
{
    func passTheData(data: [ZipCode])
}
