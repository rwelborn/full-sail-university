//
//  SearchTableViewCell.swift
//  WelbornEdwardCE06
//
//  Created by Roy Welborn on 6/21/20.
//  Copyright © 2020 Roy Welborn. All rights reserved.
//

import UIKit

class SearchTableViewCell: UITableViewCell {

    /* Label Declarations */
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var populationLabel: UILabel!
    @IBOutlet weak var zipLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
