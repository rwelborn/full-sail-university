//
//  RedditPosts.swift
//  WelbornEdwardCE08
//
//  Created by Roy Welborn on 9/18/20.
//  Copyright © 2020 Roy Welborn. All rights reserved.
//

import Foundation

class RedditPosts: Codable {
    // Stored properties
    let title: String
    let author: String
    var thumbnailString: String
    
    // Optional Initializer
    init?(jsonObject: [String: Any]) {
        // Parse through JSON object to get RedditPost object values
        guard let redditdata = jsonObject["data"] as? [String: Any],
            let redditTitle = redditdata["title"] as? String,
            let redditAuthor = redditdata["author"] as? String,
            let hasThumbnail = redditdata["thumbnail"] as? String
            else{return nil}
        
        self.title = redditTitle
        self.author = redditAuthor
        self.thumbnailString = hasThumbnail
    }
}
