//
//  ViewController.swift
//  WelbornEdwardCE08
//
//  Created by Roy Welborn on 09/09/20.
//  Copyright © 2020 Roy Welborn. All rights reserved.
//

import UIKit

/* global variable to change userDefaults.standard to defaults */
var defaults = UserDefaults.standard

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    /* TableView Declaration */
    @IBOutlet weak var redditTableView: UITableView!
    
    /* Variable Declarations */
    var subreddits = [Subreddit]()
    
    /* default Subbreddit for first time run */
    let defaultSubreddit = "ImaginarySteampunk"

    /* MARK: VeiwDidLoad */
    override func viewDidLoad() {
        super.viewDidLoad()
        
        /* Set tableView dataSource to itself */
        redditTableView.dataSource = self
        
        /* Page title setting */
        navigationItem.title = "All Subreddits"
        
        /*  Custom Back button */
        self.navigationController?.navigationBar.backIndicatorImage = UIImage(named: "backbutton")
        self.navigationController?.navigationBar.backIndicatorTransitionMaskImage = UIImage(named: "backbutton")
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: UIBarButtonItem.Style.plain, target: nil, action: nil)
        
        /* Check user defaul for any table view color */
        if let hasSavedTableViewColor = defaults.getColor(forKey: "tableViewColor") {
            /* Set tableView backgroundColor if there is a value */
            redditTableView.backgroundColor = hasSavedTableViewColor
        }
        
        /* Check if the user chose to save any new reddits  */
        if let data = defaults.object(forKey: "subredditsList") as? Data {
            
            /* create the json decoder */
            let jsonDecoder = JSONDecoder()
            
            do {
                /* decode the data into an array */
                let savedSubredditsList = try jsonDecoder.decode([Subreddit].self, from: data)
                
                /* create array from json data */
                subreddits = savedSubredditsList
                
                /* update the table with the new data */
                redditTableView.reloadData()
            }
            catch {
                /* an error in decoding will cause a new list with just the default subreddit */
                BuildAndParseJson(subredditString: defaultSubreddit)
            }
        }
        else {
            /* any other errors will cause the array to be created with only the default subreddit */
            BuildAndParseJson(subredditString: defaultSubreddit)
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        /* set the number of sections to the data array */
        return subreddits.count
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        /* set the header to the subreddits in the data array */
        return subreddits[section].subreddit
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 60
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        /* sets the number of rows in the table to the number of posts data in the array */
        return subreddits[section].redditPosts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = redditTableView.dequeueReusableCell(withIdentifier: "reuseID", for: indexPath)
        
        /* check to determine if the user changed the table color in user defaults */
        if let hasSavedTableCellColor = defaults.getColor(forKey: "tableCellColor") {
            /* set bacground value from userdefaults */
            cell.backgroundColor = hasSavedTableCellColor
        }
        /* get each subreddit section */
        let redditPosts = subreddits[indexPath.section].redditPosts
        /* get each post per section */
        let redditPost = redditPosts[indexPath.row]
        
        cell.textLabel?.text = redditPost.title
        cell.detailTextLabel?.text = redditPost.author
        /* validate thumbnailString has "https" string */
        if redditPost.thumbnailString.contains("https") {
            /* validate to make sure the string has the correct URL format */
            if let validURL = URL(string: redditPost.thumbnailString) {
                do {
                    /* try to set the imageView to the thumbnail string */
                    let data = try Data(contentsOf: validURL)
                    cell.imageView?.image = UIImage(data: data)
                }
                catch {
                    print(error.localizedDescription)
                }
            }
        }
        
        /* see if there is a user default saved for vthe tableViewCell text color */
        if let hasSavedTableTextColor = defaults.getColor(forKey: "tableTextColor") {
            /*  Set the tableViewCell text colors  to those of the userdefaults */
            cell.textLabel?.textColor = hasSavedTableTextColor
            cell.detailTextLabel?.textColor = hasSavedTableTextColor
        }
        
        return cell
    }
    
    /*  MARK: Button Actions */
    
    @IBAction func subredditsBarButtonTapped(_ sender: UIBarButtonItem) {
        performSegue(withIdentifier: "toSubredditsPage", sender: self)
    }
    
    @IBAction func themesBarButtonTapped(_ sender: UIBarButtonItem) {
        performSegue(withIdentifier: "toThemesPage", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        /* Pass the current subreddits to the SubredditsViewController */
        if segue.identifier == "toSubredditsPage" {
            if let destination = segue.destination as? SubredditsViewController {
                destination.subreddits = subreddits
            }
        }
    }
    
    /* MARK: Unwind Actions */
    
    /* Unwind method when "Save" button is pressed in ThemesViewController */
    @IBAction func unwindFromThemesSave(_ segue: UIStoryboardSegue) {
        if let source = segue.source as? ThemesViewController {
            /*  Set the tableView backgroundColor to that of the source.tableView backgroundColor */
            redditTableView.backgroundColor = source.colorDemoTableView.backgroundColor
            /* Create a temporary cell using the cell values from the source.tableView */
            let cell = source.colorDemoTableView.cellForRow(at: IndexPath(row: 0, section: 0))
            /* Iterate through all subredditLists cells */
            for i in 0...(subreddits.count - 1) {
                for j in 0...(subreddits[i].redditPosts.count - 1) {
                    /* Update the color scheme of the self.tableViewCell to that of the source.tableViewCell */
                    let redditCell = redditTableView.cellForRow(at: IndexPath(row: j, section: i))
                    redditCell?.backgroundColor = cell?.backgroundColor
                    redditCell?.textLabel?.textColor = cell?.textLabel?.textColor
                    redditCell?.detailTextLabel?.textColor = cell?.textLabel?.textColor
                }
            }
            
            /* Build UserDefaults.standard data using the colors */
            defaults.setColor(color: redditTableView.backgroundColor!, forKey: "tableViewColor")
            defaults.setColor(color: (cell?.backgroundColor)!, forKey: "tableCellColor")
            defaults.setColor(color: (cell?.textLabel?.textColor)!, forKey: "tableTextColor")
        }
    }
    
    /* Unwind method when "Save" button is pressed in SubredditsViewController */
    @IBAction func unwindFromSubredditsSave(_ segue: UIStoryboardSegue) {
        if let source = segue.source as? SubredditsViewController {
            /* Set the self.subredditsList to that of the source.subredditsList */
            subreddits = source.subreddits
            
            /* Update the tableView to add new data */
            redditTableView.reloadData()
            
            /* Create the default JSONEncoder */
            let jsonEncoder = JSONEncoder()
            do {
                /* Try to encode the subredditsList */
                let jsonData = try jsonEncoder.encode(subreddits)
                /* Set the encoded subredditsList as a UserDefaults.standard */
                defaults.set(jsonData, forKey: "subredditsList")
            }
            catch {
                print(error.localizedDescription)
            }
        }
    }
    
    /* function to gather and parse the reddit json data */
    func BuildAndParseJson(subredditString: String) {
        
        /* Create URLSession default Configuration */
        let config = URLSessionConfiguration.default
        
        /* Create the URLSession */
        let session = URLSession(configuration: config)
        
        if let url = URL(string: "https://www.reddit.com/r/\(subredditString)/.json") {
            /* Create the task */
            let task = session.dataTask(with: url) { (data, response, error) in
                
                /* return on any error */
                if error != nil {return}
                
                /* Check the return response, if it's status code 200, and has data */
                guard let response = response as? HTTPURLResponse,
                      response.statusCode == 200,
                      let data = data
                else{return}
                
                /* gather the JSON data */
                do {
                    /* Convert the data to a JSON object */
                    if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any] {
                        guard let redditData = json["data"] as? [String: Any],
                              let redditChildren = redditData["children"] as? [Any]
                              
                        else{return}
                        
                        let subredditObj = Subreddit(subreddit: subredditString)
                        
                        for childrenItem in redditChildren {
                            guard let object = childrenItem as? [String: Any]
                            else{continue}
                                if let redditPost = RedditPosts(jsonObject: object) {
                                    subredditObj?.redditPosts.append(redditPost)
                                }
                        }
                            self.subreddits.append(subredditObj!)
                    }
                }
                catch {
                    print("error")
                }
                /* Reload tableview as it cannot reload itself */
                DispatchQueue.main.async {
                    self.redditTableView.reloadData()
                }
            }
            /* Resume task */
            task.resume()
        }
    }

}

/* MARK: Previous Feedback

 */

