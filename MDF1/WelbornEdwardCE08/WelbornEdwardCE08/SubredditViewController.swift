//
//  SubredditViewController.swift
//  WelbornEdwardCE08
//
//  Created by Roy Welborn on 9/10/20.
//  Copyright © 2020 Roy Welborn. All rights reserved.
//

import UIKit

class SubredditsViewController: UIViewController, UITabBarDelegate, UITableViewDataSource {
    
    /* TableVioew declaration */
    @IBOutlet weak var subredditsTableView: UITableView!
    
    /* ToolBar Declaration */
    @IBOutlet weak var subredditsToolbar: UIToolbar!
    
    /* Empty the Subreddit array */
    var subreddits = [Subreddit]()

    /* Mark: ViewDidLoad */
    override func viewDidLoad() {
        super.viewDidLoad()

        /* Do any additional setup after loading the view. */
        
        subredditsTableView.dataSource = self
        
        /* Edit turns on "Select Mode" */
        subredditsTableView.allowsMultipleSelectionDuringEditing = true
        
        /* Page title setting */
        navigationItem.title = "Subreddits List"
        /* Create an "Edit" barButtonItem in the navigation bar */
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .edit, target: self, action: #selector(editTapped(_:)))
        
        print("subreddit count: \(subreddits.count.description)")
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        /* Set the number of rows in each section to the Subreddit array */
        return subreddits.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = subredditsTableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)
        
        cell.textLabel?.text = subreddits[indexPath.row].subreddit
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            /* Remove data from dataSource */
            self.subreddits.remove(at: indexPath.row)
            /* remove the data from the tableView */
            subredditsTableView.deleteRows(at: [indexPath], with: .fade)
        }
    }
    
    func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        if tableView.isEditing == true {
            print("Selecting Row: " + indexPath.row.description)
        }
        return indexPath
    }
    
    func tableView(_ tableView: UITableView, willDeselectRowAt indexPath: IndexPath) -> IndexPath? {
        if tableView.isEditing == true {
            print("Unselecting Row: " + indexPath.row.description)
        }
        return indexPath
    }
    
    @IBAction func editTapped(_ sender: UIBarButtonItem) {
        /* Enter edit mode, and exit edit mode */
        subredditsTableView.setEditing(!subredditsTableView.isEditing, animated: true)
        
        if subredditsTableView.isEditing == true {
            /* If edit mode is enabled, set leftBarButton to the .trash icon, and call trash all selected method */
            navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .trash, target: self, action: #selector(SubredditsViewController.trashAllSelected))
            /* If edit mode is enabled, set rightBarButton to .cancel, and call itself */
            navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(SubredditsViewController.editTapped(_:)))
            subredditsToolbar.isHidden = true
        }
        else {
            /* If edit mode is disabled then the left bar button will become a backBarButtonItem */
            navigationItem.leftBarButtonItem = navigationItem.backBarButtonItem
            /*  If edit mode is disabled then reset rightBarButton to .edit */
            navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .edit, target: self, action: #selector(SubredditsViewController.editTapped(_:)))
            subredditsToolbar.isHidden = false
        }
    }
    
    @IBAction func resetTapped(_ sender: UIBarButtonItem) {
        /* Remove all subreddits */
        subreddits.removeAll()
        /* Initialize with a default subreddit */
        BuildAndParseJson(subredditString: "ImaginarySteampunk")
    }
    
    @IBAction func addTapped(_ sender: UIBarButtonItem) {
        // Create UIAlertController for adding a new subreddit
        let addAlert = UIAlertController(title: "Add New Subreddit", message: "Type in your favorite subreddit", preferredStyle: .alert)
        // Add a textfield w/ placeholder
        addAlert.addTextField { (textField) in
            textField.placeholder = "Enter Subreddit"
        }
        // Add an "Add" button that will use the textField.text to populate a new subreddit
        addAlert.addAction(UIAlertAction(title: "Add", style: .default, handler: { (alert) in
            let textField = addAlert.textFields![0] as UITextField
            
            self.BuildAndParseJson(subredditString: textField.text!)
        }))
        // Add a "Cancel" button
        addAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        // Present UIAlertController
        self.present(addAlert, animated: true, completion: nil)
    }
    
    // Loop through and delete all the comics and cells that have been selected in edit mode
    @objc func trashAllSelected() {
        // Build alert to confirm deletion of selected lists
        let deleteListsAlert = UIAlertController(title: "Delete Lists", message: "Are you sure you want to delete the list(s)?", preferredStyle: .alert)
        // Add delete button
        deleteListsAlert.addAction(UIAlertAction(title: "Delete", style: .destructive, handler: { (alert) in
            // Get index of all selected rows
            if var selectedIPs = self.subredditsTableView.indexPathsForSelectedRows {
                // Sort in largest to smallest index, so we can remove items from back to front
                selectedIPs.sort { (a, b) -> Bool in
                    a.row > b.row
                }
                for indexPath in selectedIPs {
                    // Update the data (delete list) first
                    self.subreddits.remove(at: indexPath.row)
                }
                
                // Delete all the rows at once
                self.subredditsTableView.deleteRows(at: selectedIPs, with: .left)
            }
        }))
        // Add cancel button
        deleteListsAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        // Show alert
        self.present(deleteListsAlert, animated: true, completion: nil)
    }
    
    func BuildAndParseJson(subredditString: String) {
        // Create URLSession default Configuration
        let config = URLSessionConfiguration.default
        
        // Create a URLSession
        let session = URLSession(configuration: config)
        
        if let url = URL(string: "https://www.reddit.com/r/\(subredditString)/.json") {
            // Create a task
            let task = session.dataTask(with: url) { (data, response, error) in
                
                // Return if error
                if error != nil {return}
                
                // Check the response, it's status code, and the data
                guard let response = response as? HTTPURLResponse,
                    response.statusCode == 200,
                    let data = data
                    else{return}
                
                // Do - Catch to gather JSON data
                do {
                    // Convert data to a JSON object
                    if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any] {
                        guard let redditData = json["data"] as? [String: Any],
                            let redditChildren = redditData["children"] as? [Any]
                            else{return}
                        
                        let subredditObj = Subreddit(subreddit: subredditString)
                        
                        for thirdLevelItem in redditChildren {
                            guard let object = thirdLevelItem as? [String: Any]
                                else{continue}
                            
                            if let redditPost = RedditPosts(jsonObject: object) {
                                subredditObj?.redditPosts.append(redditPost)
                            }
                        }
                        
                        self.subreddits.append(subredditObj!)
                    }
                }
                catch {
                    print("error")
                }
                // Reload tableview as it cannot reload itself
                DispatchQueue.main.async {
                    self.subredditsTableView.reloadData()
                }
            }
            // Resume task
            task.resume()
        }
    }
}
