//
//  SubReddit.swift
//  WelbornEdwardCE08
//
//  Created by Roy Welborn on 9/11/20.
//  Copyright © 2020 Roy Welborn. All rights reserved.
//

import Foundation
import UIKit

class Subreddit: Codable {
    // Stored properties
    let subreddit: String
    var redditPosts = [RedditPosts]()
    
    // Initializer
    init?(subreddit: String) {
        self.subreddit = subreddit
    }
}

