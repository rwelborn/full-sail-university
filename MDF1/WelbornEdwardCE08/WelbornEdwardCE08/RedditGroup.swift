//
//  redditGroup.swift
//  WelbornEdwardCE04
//
//  Created by Roy Welborn on 6/13/20.
//  Copyright © 2020 Roy Welborn. All rights reserved.
//

import Foundation
import UIKit

class RedditGroup
{
    // Stored properties
    let title: String?
    let author : String?
    var thumbnail: UIImage?
    
    init?(title: String, author: String, thumbnailString: String){
        self.title = title
        self.author = author
        // Changed due to feedback from previous course
        if thumbnailString.contains("http"),
            let url = URL(string: thumbnailString),
            var urlComp = URLComponents(url: url, resolvingAgainstBaseURL: false)
        {
            // Change to secure server. All thumbs seem to come from "redditmedia" servers.
            // A simple borswer test proved that redditmedia servers allow secure access.
            // Using URLComponent object to quickly alter the incomming URL from http to https
            // Alternatives include whitelisting a domain with ATS if no secure version is available
            // Or disabling ATS altogether which is HIGHLY undesireable.
            urlComp.scheme = "https"
            
            if let secureURL = urlComp.url {
                
                let imageData = try? Data.init(contentsOf: secureURL)
                self.thumbnail = UIImage(data: imageData!)
                
            }
        }
    }
}
