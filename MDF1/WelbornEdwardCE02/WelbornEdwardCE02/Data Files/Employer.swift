//
//  Employee.swift
//  WelbornEdwardCE02
//
//  Created by Roy Welborn on 6/9/20.
//  Copyright © 2020 Roy Welborn. All rights reserved.
//

import Foundation
class Employer
{
    let company: String
    let responsibilities: [String]
    
    var numJobs: Int {
        return responsibilities.count
    }
    
    init(company: String, responsibilities: [String])
    {
        self.company = company
        self.responsibilities = responsibilities
    }
}
