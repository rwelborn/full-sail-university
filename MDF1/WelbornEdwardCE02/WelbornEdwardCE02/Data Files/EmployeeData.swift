//
//  EmployeeData.swift
//  WelbornEdwardCE02
//
//  Created by Roy Welborn on 5/9/20.
//  Copyright © 2020 Roy Welborn. All rights reserved.
//

import Foundation

class EmployeeData
{
    // Stored Properties
    let employeeName: String
    let userName: String
    let macAddress: String
    let current_Title: String
    let skills: [String]
    var past_Employers: [Employer]
    
    /*Computed Properties*/
    var numSkills: Int {
        return skills.count
    }

    // Initializers
    init(employeeName: String, userName: String, macAddress: String, current_Title: String, skills: [String], past_Employers: [Employer]) {
        
        self.employeeName = employeeName
        self.userName = userName
        self.macAddress = macAddress
        self.current_Title = current_Title
        self.skills = skills
        self.past_Employers = past_Employers
    }
}


