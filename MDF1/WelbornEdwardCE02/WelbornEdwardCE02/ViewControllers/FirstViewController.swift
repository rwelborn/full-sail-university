//
//  ViewController.swift
//  WelbornEdwardCE02
//
//  Created by Roy Welborn on 09/01/20.
//  Copyright © 2020 Roy Welborn. All rights reserved.
//

import UIKit

class FirstViewController: UIViewController
{
    // Variable Declarations
    var counter = 0
    var employeeData: [EmployeeData] = [] // Empty Array of Employee Data
    var skillCount: Int = 0
    var emplorerData:  [Employer] = []
    var pastJobsCount : Int = 0
    
    
    // Label Declarations
    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var employeeNameLabel: UILabel!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var macAddressLabel: UILabel!
    @IBOutlet weak var currentTitleLabel: UILabel!
    @IBOutlet weak var skillsLabel: UILabel!
    @IBOutlet weak var skillCountLabel: UILabel!
    @IBOutlet weak var pastEmployersLabel: UILabel!
    @IBOutlet weak var pastJobsLabel: UILabel!
    @IBOutlet weak var pastEmployerCountLabel: UILabel!
    @IBOutlet weak var pastResponsibilitiesCountLabel: UILabel!
    
  
    // IBOutlet Button Declarations for border and color definitions
    @IBOutlet weak var previousButton: UIButton!
    @IBOutlet weak var nextButton: UIButton!
    
    // Button Action Declarations
    @IBAction func previousButton(_ sender: UIButton)
    {
        // get previous employeeData Information, if at 0 or below get the first record
          counter = counter - 1
          if counter < 0
          {
              counter = 0
          }
          showEmployee(selection: counter)
    }
    
    @IBAction func nextButton(_ sender: Any)
    {
        // get next employeeData Information, if at last record get the first record
         counter = counter + 1
          if counter >= employeeData.count
          {
              counter = 0
          }
         showEmployee(selection: counter)
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        // Get the path to the Q4Transactions json file
         if let path = Bundle.main.path(forResource: "EmployeeData", ofType: ".json")
        {
            // create the url with the path
            let url = URL(fileURLWithPath: path)
        
            do
            {
                // create a data object from the file url
                let data = try Data.init(contentsOf: url)
                // create a json object from the binary data file
                let jsonObj = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [Any]
                // parse through json object and start instantiating the carinfo object

                Parse(jsonObject: jsonObj)
            }
            catch
            {
                print(error.localizedDescription)
            }
        }
        
        /* Button Border */
         nextButton.backgroundColor = .clear
         nextButton.layer.cornerRadius = 10
         nextButton.layer.borderWidth = 2
         nextButton.layer.borderColor = UIColor.systemRed.cgColor
         previousButton.backgroundColor = .clear
         previousButton.layer.cornerRadius = 10
         previousButton.layer.borderWidth = 2
         previousButton.layer.borderColor = UIColor.systemRed.cgColor
        
    }
    
    // Parse the json data into employeeData
    func Parse(jsonObject: [Any]?)
    {
        // Safely Bind jsonObject to the non-optional json
        guard let jsonObj = jsonObject
        else
        {
            print("Parsing Failed in Employee Data File")
            return
        }
            // Loop Through First Level Items
            
           for items in jsonObj
           {
                // Try to convert 1st level objects in a [String: Any]
                guard let object = items as? [String: Any],
                    let employeeNameData = object["employeename"] as? String,
                    let userNameData = object["username"] as? String,
                    let macAddressData = object["macaddress"] as? String,
                    let currentTitleData = object["current_title"] as? String,
                    let skilldata = object["skills"] as? [String],
                    let past_EmployersData = object["past_employers"] as? [[String: Any]]
                    
                    // if anything in guard fails, continue to next
            else { continue }
            
            // map the skills data array from the object
            let employee = EmployeeData(employeeName: employeeNameData, userName: userNameData, macAddress: macAddressData, current_Title: currentTitleData, skills: skilldata,  past_Employers: emplorerData)
            

            for data in past_EmployersData
            {
                guard let past_Companies = data["company"] as? String,
                    let past_responsibilities = data["responsibilities"] as? [String]
                else { continue }
                employee.past_Employers.append(Employer(company: past_Companies, responsibilities: past_responsibilities))
                
                print(past_responsibilities.description)
            }
            
          //  print(past_EmployersData.count)
            employeeData.append(employee)
        }
        headerLabel.text = "Employee Data for \(employeeData.count.description) employees"
             showEmployee(selection: 0)
    
    }
    func showEmployee(selection: Int)
     {
        /* Initiale variable at each show */
        var pastEmployer = ""
        var pastJobs = ""
        skillCount = 0
        var skillCountVar = 0
        var pastEmployerCount = 0
        var pastJobsCount = 0
        
        employeeNameLabel.text = "Employee Name:  \(employeeData[selection].employeeName)"
        usernameLabel.text = "UserName:  \(employeeData[selection].userName)"
        macAddressLabel.text = "MAC Address:  \(employeeData[selection].macAddress)"
        currentTitleLabel.text = "Current Title:   \(employeeData[selection].current_Title)"
        
        let skillSet = "Skills:  \(employeeData[selection].skills.joined(separator: ", "))"
         skillCountVar = employeeData[selection].skills.count
        if skillCountVar < 1
        {
            skillCountVar = 0
        }
       
        skillCountLabel.text = "Total Skills: \(skillCountVar)"
        skillsLabel.text = skillSet
        print(employeeData[selection].past_Employers.count.description)
        for employer in employeeData[selection].past_Employers {
          
            pastEmployerCount += 1 // Mark: pastEmployerCount not working
            pastEmployer += employer.company + ", "
            pastJobs = employer.responsibilities.joined(separator: ", ")
            pastJobsCount = employer.responsibilities.count
            
        }

        if pastEmployerCount < 1 {
            pastEmployerCount = 0
            pastEmployer = "N/A"
        }
        
        pastEmployersLabel.text = "Past Employers: \(pastEmployer)"
        pastEmployerCountLabel.text = "Total Past Employers: \(pastEmployerCount)"
        
        if pastJobsCount < 1 {
            pastJobsCount = 0
            pastJobs = "N/A"
        }
        
        pastJobsLabel.text = "Past Jobs: \(pastJobs)"
        pastResponsibilitiesCountLabel.text = "Past Responsibilities:   \(pastJobsCount.description)"
        
     }
}
/* MARK: Feedback from last attempt */
/*
 Patti DacostaWed Aug 12 @ 07:48 am CDT
 Good work!

 JSON Data: Excellent 5/5
 All requirements met.
 JSON Parsing: Excellent 30/30
 All requirements met.
 Model Objects: Excellent 25/25
 All requirements met.
 App Functionality: Excellent 20/20
 All requirements met.
 Code Structure & Efficiency: Excellent 20/20
 All requirements met.
 Roy, nice job on the JSON Intermediate lab. There was an issue with a missing image that you placed on the LaunchScreen, so I had to remove this from the view in order for the app to run. When adding assets to a project make sure that they are copied into the project otherwise it will only run for you. The reason for this is that instead of copying the asset into the project, a reference to the asset was created that points to a location on your machine. A good way to double-check this is to view the project through a finder window.
 */
