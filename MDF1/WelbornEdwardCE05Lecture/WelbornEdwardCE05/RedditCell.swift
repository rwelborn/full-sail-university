//
//  RedditCell.swift
//  WelbornEdwardCE05
//
//  Created by Roy Welborn on 6/16/20.
//  Copyright © 2020 Roy Welborn. All rights reserved.
//

import UIKit

class RedditCell: UITableViewCell {

    /* Label and Image Declarations */
    @IBOutlet weak var thumbnail: UIImageView!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var author: UILabel!
    @IBOutlet weak var comments: UILabel!
    @IBOutlet weak var score: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
