//
//  VC_Extension.swift
//  TableViewIntermediate
//
// "⬆︎"

/*
 Reddit Icon made by Freepik from www.flaticon.com
 http://www.freepik.com/
 */

import Foundation


extension TableViewController{


    func downloadJSON(atURL urlString: String) {
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        if let validURL = URL(string: urlString) {

            let task = session.dataTask(with: validURL, completionHandler: { (opt_data, opt_response, opt_error) in

                //Bail Out on error
                if opt_error != nil { assertionFailure(); return }

                //Check the response, statusCode, and data
                guard let response = opt_response as? HTTPURLResponse,
                    response.statusCode == 200,
                    let data = opt_data
                    else { assertionFailure(); return
                }

                do {
                    //De-Serialize data object
                    if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any] {

                        guard let outerData = json["data"] as? [String: Any],
                            //Array of Objects in JSON, Array of Dictionaries in Swift
                            let children = outerData["children"] as? [[String:Any]]
                            else { assertionFailure(); return
                        }

                        //Loop through Children Array. Here is where get to the actual post.
                        for child in children {
                            guard let post = child["data"] as? [String: Any]
                                else {assertionFailure(); return
                            }

                            do {
                                //Create and Append the current post object
                                try self.posts.append(Post(jsonRedditPost: post))
                            }
                            catch {
                                assertionFailure("Post Creation Failed.")
                            }
                        }
                    }
                }
                catch {
                    print(error.localizedDescription)
                    assertionFailure();
                }

                //Do UI Stuff
//                DispatchQueue.main.async {
//                    self.filterPostsBySubreddit()
//                    self.tableView.reloadData()
//                }

            })
            task.resume()
        }
    }

    
    func filterPostsBySubreddit() {

        filteredPosts[0] = posts.filter({ $0.subReddit == "swift" })
        filteredPosts[1] = posts.filter({ $0.subReddit == "ios" })
        filteredPosts[2] = posts.filter({ $0.subReddit == "iosgaming" })

    }

}
