//
//  ViewController.swift
//  WelbornEdwardCE05
//
//  Created by Roy Welborn on 6/17/20.
//  Copyright © 2020 Roy Welborn. All rights reserved.
//

import UIKit

class DetailsViewController: UIViewController {

    /* Variable Declarations */
    var post: Post!
    
    /* Label Declarations */
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    /* TextView Declarations */
    @IBOutlet weak var textView: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if post != nil
        {
            titleLabel.text = post.title
            dateLabel.text = post.date.description
            textView.text = post.postText
        }
        
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
