//
//  TableViewController.swift
//  WelbornEdwardCE05
//
//  Created by Roy Welborn on 6/16/20.
//  Copyright © 2020 Roy Welborn. All rights reserved.
//

import UIKit

class TableViewController: UITableViewController {

    // variable Declaration
    var posts = [Post]()
    var filteredPosts = [[Post](), [Post](), [Post]()]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        downloadJSON(atURL: "https://www.reddit.com/r/swift/.json")
        downloadJSON(atURL: "https://www.reddit.com/r/ios/.json")
        downloadJSON(atURL: "https://www.reddit.com/r/iosgaming/.json")
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 3
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return filteredPosts[section].count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "CellID01", for: indexPath) as? RedditCell
            else { return tableView.dequeueReusableCell(withIdentifier: "CellID01", for: indexPath) }
        
        let currentPost = filteredPosts[indexPath.section][indexPath.row]
        
        cell.title.text = currentPost.title
        cell.author.text = currentPost.author
        cell.comments.text = "c: " + currentPost.numComments.description
        cell.score.text = "⬆︎: " + currentPost.score.description
        
        if let img = currentPost.thumbImage
        {
            cell.thumbnail?.image = img
        }
        return cell
        
    }
    
    // Mark - Header Methods
 
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String?
    {
        switch section {
        case 0:
            return "Swift"
        case 1:
            return "iOS"
        case 2:
            return "iosGaming"
        default:
            return "Ooops, Should not be here"
        }
    }

    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
 
    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if let indexPath = tableView.indexPathForSelectedRow
        {
            let postToSend = filteredPosts[indexPath.section][indexPath.row]
            
            if let destination = segue.destination as? DetailsViewController
            {
                destination.post = postToSend
            }
        }
        
    }
    

}
