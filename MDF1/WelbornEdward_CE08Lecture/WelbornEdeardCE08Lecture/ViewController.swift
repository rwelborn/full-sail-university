//
//  ViewController.swift
//  WelbornEdeardCE08Lecture
//
//  Created by Roy Welborn on 6/26/20.
//  Copyright © 2020 Roy Welborn. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITextFieldDelegate {

    /* Variable Declarations */
    
    /* UIView Declarations */
    @IBOutlet weak var colorView: UIView!
    
    /* TextField Declarations */
    @IBOutlet weak var colorTexrtField: UITextField!
    
    /* Label Declarations */
    @IBOutlet weak var redLabel: UILabel!
    @IBOutlet weak var greenLabel: UILabel!
    @IBOutlet weak var blueLabel: UILabel!
    
    /* Slider Outlet Declarations */
    @IBOutlet weak var redSlider: UISlider!
    @IBOutlet weak var greenSlider: UISlider!
    @IBOutlet weak var blueSider: UISlider!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        colorTexrtField.delegate = self
        for slider in [redSlider, greenSlider, blueSider]
        {
            slider?.value = 0
        }
    }

    /* Slider Action Delarations */
    @IBAction func sliderDidChange(_ sender: UISlider) {
        
         switch sender.tag {
            case 0:
                // red Slider
                // update label
                redLabel.text = sender.value.description
                // update view
                colorView.layer.backgroundColor = UIColor(displayP3Red: CGFloat(sender.value), green: CGFloat(greenSlider.value), blue: CGFloat(blueSider.value), alpha: 1).cgColor
            case 1:
                // green slider
                // update label
                greenLabel.text = sender.value.description
                // update view
                colorView.layer.backgroundColor = UIColor(displayP3Red: CGFloat(redSlider.value), green: CGFloat(sender.value), blue: CGFloat(blueSider.value), alpha: 1).cgColor
            case 2:
                // blue slider
                // update label
                blueLabel.text = sender.value.description
                // update view
                colorView.layer.backgroundColor = UIColor(displayP3Red: CGFloat(redSlider.value), green: CGFloat(greenSlider.value), blue: CGFloat(sender.value), alpha: 1).cgColor
            default:
                print("forbidden area, you should not be here")
            }
            // update th colorView's view
            colorView.layer.backgroundColor = UIColor(displayP3Red: CGFloat(redSlider.value), green: CGFloat(greenSlider.value), blue: CGFloat(blueSider.value), alpha: 1).cgColor
    }
    
    /* Buttons Declarations */
    @IBAction func loadButton(_ sender: UIButton) {
        
       if let savedText = UserDefaults.standard.object(forKey: "colorText") as? String{
        colorTexrtField.text = savedText
        }
    }
    
    @IBAction func saveButton(_ sender: UIButton) {
        
        /* Access our standard set of user defaults and save the text with a known key */
        UserDefaults.standard.set(colorTexrtField.text, forKey: "colorText")
        
        /* Clear the textfield */
        colorTexrtField.text = ""
    }
    /* Keyboard will close when return key pressed */
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

