//
//  settingsViewController.swift
//  WelbornEdwardCE08
//
//  Created by Roy Welborn on 7/31/20.
//  Copyright © 2020 Roy Welborn. All rights reserved.
//

import UIKit

class settingsViewController: UIViewController {

    /* Label Declaration */
    @IBOutlet weak var redLabel: UILabel!
    @IBOutlet weak var greenLabel: UILabel!
    @IBOutlet weak var blueLabel: UILabel!
    
    /* Button IBOutlets for Color */
    @IBOutlet weak var titleButton: UIButton!
    @IBOutlet weak var authorButton: UIButton!
    @IBOutlet weak var viewButton: UIButton!
    
    /* Slider Declaration*/
    @IBOutlet weak var redSlider: UISlider!
    @IBOutlet weak var greenSlider: UISlider!
    @IBOutlet weak var blueSlider: UISlider!
    
    /* Button Declaration */
    @IBAction func backButton(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        for slider in [redSlider, greenSlider, blueSider]
        {
            slider?.value = 0
        }
    }
    
    @IBAction func LabelSelected(_ sender: UISlider) {
        switch sender.tag {
           case 0:
               // red Slider
               // update label
               redLabel.text = sender.value.description
               // update view
               titleButton.titleLabel?.textColor = UIColor(displayP3Red: CGFloat(sender.value), green: CGFloat(greenSlider.value), blue: CGFloat(blueSlider.value), alpha: 1)
           case 1:
               // green slider
               // update label
               greenLabel.text = sender.value.description
               // update view
               authorButton.titleLabel?.textColor = UIColor(displayP3Red: CGFloat(redSlider.value), green: CGFloat(sender.value), blue: CGFloat(blueSlider.value), alpha: 1)
           case 2:
               // blue slider
               // update label
               blueLabel.text = sender.value.description
               // update view
               viewButton.titleLabel?.textColor = UIColor(displayP3Red: CGFloat(redSlider.value), green: CGFloat(greenSlider.value), blue: CGFloat(sender.value), alpha: 1)
           default:
               print("forbidden area, you should not be here")
           }
           
    }
}
