//
//  UserDefaults_EXT.swift
//  WelbornEdeardCE08Lecture
//
//  Created by Roy Welborn on 6/27/20.
//  Copyright © 2020 Roy Welborn. All rights reserved.
//

import Foundation
import UIKit

extension UserDefaults
{
    /* Save UIColor as a data object */
    func set(color: UIColor, forkey key: String)
    {
        /* convert UIColor object into a data object by archiving */
        let binaryData = try? NSKeyedArchiver.archivedData(withRootObject: color, requiringSecureCoding: false)
        
        /* save binary data to usefdefaults */
        
        self.set(color: binaryData, forkey: key)
    }
    
    /* Get UIColor from saved defaults with a key */
    func color(forkey key: String) -> UIColor?
    {
        /* Check for valid data */
        if let binaryData = data(forKey: key)
        {
            /* is the data a UIColor */
            if let color = NSKeyedUnarchiver.unarchiveObject(with: binaryData) as? UIColor
            {
                return color
            }
            
        }
        /* If for some reason we didn't  make it to the color return, then someting is wrong witht he data */
        return nil
    }
}
