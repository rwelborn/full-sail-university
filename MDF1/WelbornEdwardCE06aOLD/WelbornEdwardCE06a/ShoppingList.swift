//
//  ShoppingList.swift
//  WelbornEdwardCE06a
//
//  Created by Roy Welborn on 8/21/20.
//  Copyright © 2020 Roy Welborn. All rights reserved.
//

import Foundation

class ShoppingList
{
    /* Stored Properties */
    var list : String = ""
    var itemsNeeded = [String]()
    var itemsPurchased = [String]()
    
    /* Computed properties */
     var numberOfItems: Int {
         return itemsNeeded.count
     }
     
     /* Initializers */
     init(store: String, itemsNeeded: [String] = [String]()) {
         self.list = store
         self.itemsNeeded = itemsNeeded
     }
    
    /* Optional initializer */
    init?(jsonObject: [String: Any]) {
        guard let list = jsonObject["list"] as? String,
            let itemsNeeded = jsonObject["items"] as? [Any]
            else{return nil}
        
        self.list = list
        for data in itemsNeeded {
            guard let object = data as? String else{continue}
            self.itemsNeeded.append(object)
        }
    }
}
