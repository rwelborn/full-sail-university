//
//  TableViewController.swift
//  WelbornEdwardCE06a
//
//  Created by Roy Welborn on 8/21/20.
//  Copyright © 2020 Roy Welborn. All rights reserved.
//

import UIKit

class TableViewCell: UITableViewCell {
    
    // MARK: Preparations
    @IBOutlet weak var listNameLabel: UILabel!
    @IBOutlet weak var numberOfItemsLabel: UILabel!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
