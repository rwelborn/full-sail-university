//
//  TableViewController.swift
//  WelbornEdwardCE06a
//
//  Created by Roy Welborn on 8/21/20.
//  Copyright © 2020 Roy Welborn. All rights reserved.
//
import UIKit

class SectionHeaderViewController: UITableViewHeaderFooterView {
    
    // MARK: Preparations
    @IBOutlet weak var sectionHeaderLabel: UILabel!
    @IBOutlet weak var numberOf_Label: UILabel!
    @IBOutlet weak var numberOfListsLabel: UILabel!
    @IBOutlet weak var headerButton: UIButton!
    
}
