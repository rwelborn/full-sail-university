//
//  ViewController.swift
//  EdwardWelbornCE01
//
//  Created by Roy Welborn on 09/01/20.
//  Copyright © 2020 Roy Welborn. All rights reserved.
//

import UIKit

class FirstViewController: UIViewController
{

    /* Label Declarations */
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var makeLabel: UILabel!
    @IBOutlet weak var modelLabel: UILabel!
    @IBOutlet weak var engineLabel: UILabel!
    @IBOutlet weak var extColorLabel: UILabel!
    @IBOutlet weak var mileageLabel: UILabel!
    @IBOutlet weak var seatingLabel: UILabel!
    @IBOutlet weak var costLabel: UILabel!
    @IBOutlet weak var hybridLabel: UILabel!
    @IBOutlet weak var zeroToSixtyLabel: UILabel!
    @IBOutlet weak var intColorLabel: UILabel!
    @IBOutlet weak var cargoSpaceLabel: UILabel!
    
    
    /* Variable Declarations */
    var counter = 0
    var jsonCount = 0
    var carInfo = [CarInfo]()
    
    /* IBOutlet Declarastions for Buttons to define borders and colors */
    @IBOutlet weak var previousButton: UIButton!
    @IBOutlet weak var nextButton: UIButton!
    
    /* Action Button Declarations */
    @IBAction func previousButton(_ sender: UIButton)
    {
        /* Show Previous data item on screen */
        counter = counter - 1
        if counter < 0
        {
            counter = 0
        }
        showCar(selection: counter)
    }
    
    @IBAction func nextButton(_ sender: UIButton)
    {
        /* show next data item on screen */
        counter = counter + 1
         if counter >= carInfo.count
         {
             counter = 0
         }
        showCar(selection: counter)
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        /* Get path to the json file */
        if let path = Bundle.main.path(forResource: "CarInfo", ofType: ".json")
        {
            /* create the url with the path */
            let url = URL(fileURLWithPath: path)
        
            do
            {
                /* create a data object from the file url */
                let data = try Data.init(contentsOf: url)
                /* create a json object from the binary data file */
                let jsonObj = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [Any]
                /* parse through json object and start instantiating the carinfo object */
                Parse(jsonObject: jsonObj)
                jsonCount = jsonObj!.count
            }
            catch
            {
                print(error.localizedDescription)
            }
        }
        /* Button Border */
        nextButton.backgroundColor = .clear
        nextButton.layer.cornerRadius = 10
        nextButton.layer.borderWidth = 2
        nextButton.layer.borderColor = UIColor.systemRed.cgColor
        previousButton.backgroundColor = .clear
        previousButton.layer.cornerRadius = 10
        previousButton.layer.borderWidth = 2
        previousButton.layer.borderColor = UIColor.systemRed.cgColor
    }
    
    /* method to convert our any into usable type, to instantiate carinfo object */
    func Parse(jsonObject: [Any]?)
    {
        /* Safely Bind jsonObject to the non-optional json */
        if let jsonObj = jsonObject
        {
            /* Loop Through items */
           for items in jsonObj
           {
                guard let object = items as? [String: Any],
                    let carMake = object["Make"] as? String,
                    let carModel = object["Model"] as? String,
                    let engineSize = object["Engine"] as? [String],
                    let carExtColor = object["ExtColor"] as? String,
                    let carMileage = object["Mileage"] as? Double,
                    let carSeats = object["Seats"] as? Int,
                    let carCost = object["Cost"] as? Double,
                    let carHybrid = object["Hybrid"] as? Bool,
                    let zeroToSixty = object["0to60"] as? Double,
                    let carIntColor = object["IntColor"] as? String,
                    let carCargoSpace = object["CargoSpace"] as? Double
            else { continue }
            
            carInfo.append(CarInfo(make: carMake, model: carModel, engine: engineSize, extColor: carExtColor, mileage: carMileage, seats: carSeats, cost: carCost, hybrid: carHybrid, zeroToSixty: zeroToSixty, intColor: carIntColor, cargoSpace: carCargoSpace))
            }
            let totalCars = carInfo.count + 1
            titleLabel.text = "Car Info for \(totalCars.description) vehicles"
            showCar(selection: 0)
        }
    }
    
    /* method to show car info on the form */
    func showCar(selection: Int)
    {
        makeLabel.text = "Make: \(carInfo[selection].make)"
        modelLabel.text = "Model: \(carInfo[selection].model)"
        
        let engineDisplacement =  carInfo[selection].engine.joined(separator: ", ")
        engineLabel.text = "Engine: \(engineDisplacement)"
        
        extColorLabel.text = "Exterior Color: \(carInfo[selection].extColor)"
        
        let estMileage = String(format: "%.02f miles per gallon", carInfo[selection].mileage)
        mileageLabel.text = "Est. Mileage: \(estMileage)"
        
        seatingLabel.text = "Seating: \(String(carInfo[selection].seats))"
        
        let cost = String(format: "$%.02f", carInfo[selection].cost)
        costLabel.text = "Cost: \(cost)"
        
        if carInfo[selection].hybrid == false
        {
            hybridLabel.text = "Hybrid: No"
        }
        else
        {
            hybridLabel.text = "Hybrid: Yes"
        }
        
        let zeroToSixty = String(format: "%.02f seconds", carInfo[selection].zeroToSixty)
        zeroToSixtyLabel.text = "Zero To Sixty: \(zeroToSixty)"
        
        intColorLabel.text = "Interior Color: \(carInfo[selection].intColor)"
        
        let cargoSpace = String(format: "%.02f cubic feet", carInfo[selection].cargoSpace)
        cargoSpaceLabel.text = "Cargo Space: \(cargoSpace)"
        
    }
}

/* Feedback from previous attempt */
/*
 Resubmission accepted.

 JSON Data File: Excellent 15/15
 All requirements met.
 JSON Parsing: Excellent 25/25
 All requirements met.
 Model Objects: Excellent 20/20
 All requirements met.
 App Functionality: Excellent 20/20
 All requirements met.
 Code Structure & Efficiency: Excellent 20/20
 All requirements met.
 Roy, your resubmission of CE:01 has been accepted without changes as this work received 100% during the 202007 term.
 
 */

