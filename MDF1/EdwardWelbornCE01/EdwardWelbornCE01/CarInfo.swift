//
//  CarInfo.swift
//  EdwardWelbornCE01
//
//  Created by Roy Welborn on 5/7/20.
//  Copyright © 2020 Roy Welborn. All rights reserved.
//

import Foundation

class CarInfo: Codable
{
    
    /* Stored Procedures */
    var make: String
    var model: String
    var engine: [String]
    var extColor: String
    var mileage: Double
    var seats: Int
    var cost: Double
    var hybrid: Bool
    var zeroToSixty: Double
    var intColor: String
    var cargoSpace: Double
    
    /* Initializer */
    
    init(make: String, model: String, engine: [String], extColor: String, mileage: Double, seats: Int, cost: Double, hybrid: Bool, zeroToSixty: Double, intColor: String, cargoSpace: Double)
    {
        self.make = make
        self.model = model
        self.extColor = extColor
        self.engine = engine
        self.mileage = mileage
        self.seats = seats
        self.cost = cost
        self.hybrid = hybrid
        self.zeroToSixty = zeroToSixty
        self.intColor = intColor
        self.cargoSpace = cargoSpace
    }
}
