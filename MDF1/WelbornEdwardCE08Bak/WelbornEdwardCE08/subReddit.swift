//
//  subReddit.swift
//  WelbornEdwardCE08
//
//  Created by Roy Welborn on 8/27/20.
//  Copyright © 2020 Roy Welborn. All rights reserved.
//

import Foundation

class Subreddit: Codable {
    // Stored properties
    let subreddit: String
    var redditgroup = [redditGroup]()
    
    // Initializer
    init?(subreddit: String) {
        self.subreddit = subreddit
    }
}
