//
//  redditGroup.swift
//  WelbornEdwardCE04
//
//  Created by Roy Welborn on 6/13/20.
//  Copyright © 2020 Roy Welborn. All rights reserved.
//

import Foundation
import UIKit

class redditGroup: Codable
{
    /* Stored Properties */
     let title: String
     let author: String
     var thumbnailString: String
    
    // Optional Initializer
     init?(jsonObject: [String: Any]) {
         // Parse through JSON object to get RedditPost object values
         guard let redditData = jsonObject["data"] as? [String: Any],
             let redditTitle = redditData["title"] as? String,
             let redditAuthor = redditData["author"] as? String,
             let hasThumbnail = redditData["thumbnail"] as? String
             else{return nil}
         
         self.title = redditTitle
         self.author = redditAuthor
         self.thumbnailString = hasThumbnail
     }
}
