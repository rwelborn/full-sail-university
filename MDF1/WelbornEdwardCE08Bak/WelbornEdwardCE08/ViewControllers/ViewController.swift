//
//  ViewController.swift
//  WelbornEdwardCE04
//
//  Created by Roy Welborn on 8/19/20.
//  Copyright © 2020 Roy Welborn. All rights reserved.
//

import UIKit

let Default = UserDefaults.standard

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    /* Variable Declaration */
    var subReddit = [Subreddit]()
    
    
    /* Label Declaration */
    @IBOutlet weak var subRedditLabel: UILabel!
    
    /* Toolbar Button Declarations */
    @IBAction func subredditButton(_ sender: Any) {
        performSegue(withIdentifier: "toSubredditsPage", sender: self)
    }
    @IBAction func themeButton(_ sender: Any) {
        performSegue(withIdentifier: "toThemesPage", sender: self)
    }
    
    /* TableView Declaration */
    @IBOutlet weak var redditTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        super.viewDidLoad()
        navigationItem.title = "Reddit Posts"
        // Custom Back button
        self.navigationController?.navigationBar.backIndicatorImage = UIImage(named: "BackArrow")
        self.navigationController?.navigationBar.backIndicatorTransitionMaskImage = UIImage(named: "BackArrow")
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: UIBarButtonItem.Style.plain, target: nil, action: nil)
        redditTableView.dataSource = self
        redditTableView.delegate = self
        getRedditJSON(whichReddit: "ImaginarySteampunk")
    }
    

    
    /* tableview callback protocols */
    
    func numberOfSections(in tableView: UITableView) -> Int {
        // Set number of sections to the subredditsList count
        return subReddit.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        /* return the count of the data from the reddit json */
        return subReddit[section].redditgroup.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = redditTableView.dequeueReusableCell(withIdentifier: "reuseID", for: indexPath)
        
        // Check if there is a saved tableViewCell color
        if let hasSavedTableCellColor = Default.getColor(forKey: "tableCellColor") {
            // Set the cell backgroundColor to that of the saved tableViewCell color
            cell.backgroundColor = hasSavedTableCellColor
        }
        // Get the subreddit in each section
        let redditPosts = subReddit[indexPath.section].redditgroup
        // Get the posts in each subreddit
        let redditPost = redditPosts[indexPath.row]
        
        cell.textLabel?.text = redditPost.title
        cell.detailTextLabel?.text = redditPost.author
        // Check if thumbnailString is a valid "http" string
        if redditPost.thumbnailString.contains("http") {
            // Test if the string can be a valid URL
            if let validURL = URL(string: redditPost.thumbnailString) {
                do {
                    // Attempt to set the cell imageView to that of the thumbnail
                    let data = try Data(contentsOf: validURL)
                    cell.imageView?.image = UIImage(data: data)
                }
                catch {
                    print(error.localizedDescription)
                }
            }
        }
        
        // Check if there is a saved tableViewCell text color
        if let hasSavedTableTextColor = Default.getColor(forKey: "tableTextColor") {
            // Set the tableViewCell text colors
            cell.textLabel?.textColor = hasSavedTableTextColor
            cell.detailTextLabel?.textColor = hasSavedTableTextColor
        }
        
        return cell
    }
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        // Set section header title to subreddit name
        return subReddit[section].subreddit.description
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 25
    }
    
    // MARK: Actions
     
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
         // Pass the current subreddits available to the SubredditsViewController
         if segue.identifier == "toSubredditsPage" {
             if let destination = segue.destination as? SubRedditViewController {
                 destination.subreddits = subReddit
             }
         }
     }
     
     // Unwind method when "Save" button is pressed in ThemesViewController
     @IBAction func unwindFromThemesSave(_ segue: UIStoryboardSegue) {
         if let source = segue.source as? ThemeViewController {
             // Set the tableView backgroundColor to that of the source.tableView backgroundColor
             redditTableView.backgroundColor = source.colorDemoTableView.backgroundColor
             // Create a temporary cell using the cell values from the source.tableView
             let cell = source.colorDemoTableView.cellForRow(at: IndexPath(row: 0, section: 0))
             // Iterate through all subredditLists cells
             for i in 0...(subReddit.count - 1) {
                 for j in 0...(subReddit[i].redditgroup.count - 1) {
                     // Update the color scheme of the self.tableViewCell to that of the source.tableViewCell
                     let redditCell = redditTableView.cellForRow(at: IndexPath(row: j, section: i))
                     redditCell?.backgroundColor = cell?.backgroundColor
                     redditCell?.textLabel?.textColor = cell?.textLabel?.textColor
                     redditCell?.detailTextLabel?.textColor = cell?.textLabel?.textColor
                 }
             }
             
             // Build UserDefaults.standard data using the colors
             Default.setColor(color: redditTableView.backgroundColor!, forKey: "tableViewColor")
             Default.setColor(color: (cell?.backgroundColor)!, forKey: "tableCellColor")
             Default.setColor(color: (cell?.textLabel?.textColor)!, forKey: "tableTextColor")
         }
     }
     
     // Unwind method when "Save" button is pressed in SubredditsViewController
     @IBAction func unwindFromSubredditsSave(_ segue: UIStoryboardSegue) {
         if let source = segue.source as? SubRedditViewController {
             // Set the self.subredditsList to that of the source.subredditsList
             subReddit = source.subreddits
             
             // Update the tableView to add new data
             redditTableView.reloadData()
             
             // Create a default JSONEncoder
             let jsonEncoder = JSONEncoder()
             do {
                 // Try to encode the subredditsList
                 let jsonData = try jsonEncoder.encode(subReddit)
                 // Set the encoded subredditsList as a UserDefaults.standard
                Default.set(jsonData, forKey: "subreddits")
             }
             catch {
                 print(error.localizedDescription)
             }
         }
     }    
}

