//
//  ShoppingList.swift
//  WelbornEdwardCE06
//
//  Created by Roy Welborn on 8/22/20.
//  Copyright © 2020 Roy Welborn. All rights reserved.
//

import Foundation
class ShoppingList {
    
    // Stored properties
    var listName: String = ""
    var listItems = [String]()
    var itemsPurchased = [String]()
    
    // Computed properties
    var numberOfListItems: Int {
        return listItems.count
    }
    
    // Initializer
    init(listName: String, listItems: [String] = [String]()) {
        self.listName = listName
        self.listItems = listItems
    }
}
