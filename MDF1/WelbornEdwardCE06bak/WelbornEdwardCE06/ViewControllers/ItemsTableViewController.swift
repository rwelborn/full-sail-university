//
//  ItemsTableViewController.swift
//  WelbornEdwardCE06
//
//  Created by Roy Welborn on 8/22/20.
//  Copyright © 2020 Roy Welborn. All rights reserved.
//

import UIKit

class ItemsTableViewController: UITableViewController {

     
    /* Variable Declarations */
    var list: ShoppingList!
 //   var listItem : [ShoppingList]()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        /* enabling multiple selection */
        tableView.allowsMultipleSelectionDuringEditing = true
        /* Set the title to the name of each list */
        navigationItem.title = "\(list.listName) List Details"
        /* Add the righthand Bar Button to the navigationBar, adds a button calls editTapped method */
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .edit, target: self, action: #selector(ItemsTableViewController.editTapped(_:)))
        /* Registers the section header xib to the itemstableviewcontroller */
        tableView.register(UINib(nibName: "SectionHeader", bundle: nil), forHeaderFooterViewReuseIdentifier: "reuseSectionHeader")
        tableView.allowsSelectionDuringEditing = true
    }
    
    /* Method that handles the edit button when pressed */
    @IBAction func editTapped(_ sender: UIButton) {
         /* Enters and exits edit mode depending on the button press */
         tableView.setEditing(!tableView.isEditing, animated: true)
         
         if tableView.isEditing == true {
             /* when edit mode is enabled, this will set the leftBarButton to .trash (trashcan icon), and calls the trash all method for the items selected */
             navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .trash, target: self, action: #selector(ItemsTableViewController.trashAllSelected))
              /* When edit mode is enabled, this will set the rightBarButton to .cancel */
             navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(ItemsTableViewController.editTapped(_:)))
         }
         else {
             /* When edit mode is disabled the leftBarButton will not be shown */
             navigationItem.leftBarButtonItem = nil
             /* When edit mode is disabled the rightBarButton to the default .edit */
             navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .edit, target: self, action: #selector(ItemsTableViewController.editTapped(_:)))
         }
     }
    
    /* Loop through and delete all the comics and cells that have been selected in edit mode */
     @objc func trashAllSelected() {
         /* Build the custom alert to confirm the deletion of items */
         let deleteListsAlert = UIAlertController(title: "Delete Items", message: "Are you sure you want to delete items from the list?", preferredStyle: .alert)
         /*  Add the delete button */
         deleteListsAlert.addAction(UIAlertAction(title: "Delete", style: .destructive, handler: { (alert) in
             /* Get index of all selected rows */
             if var selectedIPs = self.tableView.indexPathsForSelectedRows {
                 /* Sort largest to smallest index */
                 selectedIPs.sort { (a, b) -> Bool in
                     a.row > b.row
                 }
                
                 for indexPath in selectedIPs {
                     /* Update the data */
                    self.list.listItems.remove(at: indexPath.row)
                 }
                 
                 /* Delete all the selected rows */
                 self.tableView.deleteRows(at: selectedIPs, with: .left)
                 /* Update custom header */
                 let header = self.tableView.headerView(forSection: 0) as? SectionHeaderViewController
                header?.numberOfListsLabel.text = self.list.listItems.count.description
             }
         }))
         /* add the cancel button */
         deleteListsAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
         /* show the custom alert */
         self.present(deleteListsAlert, animated: true, completion: nil)
     }
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        /* Configure  sections in tableview for items needed and items purchased */
        return 2
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        /*  switch method to show the count of each list item or purchased item */
        var numberOfRows = 0
        switch section {
        case 0:
            numberOfRows += list.listItems.count
        case 1:
            numberOfRows += list.itemsPurchased.count
        default:
            numberOfRows = 0
        }
        return numberOfRows
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)

        /* Configuring the cell(s) */
        switch indexPath.section {
        case 0:
            cell.textLabel?.text = list.listItems[indexPath.row]
        case 1:
            cell.textLabel?.text = list.itemsPurchased[indexPath.row]
        default:
            cell.textLabel?.text = ""
        }

        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        /* set the custom header height */
        return 75
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        /* Set the custom section header to the sectionheaderviewcontroller xib we created */
        let itemsHeader = tableView.dequeueReusableHeaderFooterView(withIdentifier: "reuseSectionHeader") as? SectionHeaderViewController
        
        /* Custom Header button declaration */
        itemsHeader?.headerButton.isHidden = false
        itemsHeader?.headerButton.addTarget(self, action: #selector(addTapped), for: .touchUpInside)
        itemsHeader?.numberOf_Label.text = "Number of Items:"
        /* Update the header labels for each section by which list it is */
        switch section {
        case 0:
            itemsHeader?.sectionHeaderLabel.text = "Items Needed"
            itemsHeader?.numberOfListsLabel.text = list.listItems.count.description
        case 1:
            itemsHeader?.sectionHeaderLabel.text = "Purchased Items"
            itemsHeader?.numberOfListsLabel.text = list.itemsPurchased.count.description
        default:
            itemsHeader?.sectionHeaderLabel.text = ""
            itemsHeader?.numberOf_Label.text = ""
            itemsHeader?.numberOfListsLabel.text = ""
        }
        
        return itemsHeader
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        /* Using a switch statement to find the cell selection in each section, add it to the opposite section, then remove it from its initial section */
        if tableView.isEditing != true {
            switch indexPath.section {
            case 0:
                list.itemsPurchased.append(list.listItems[indexPath.row])
                list.listItems.remove(at: indexPath.row)
            case 1:
                list.listItems.append(list.itemsPurchased[indexPath.row])
                list.itemsPurchased.remove(at: indexPath.row)
            default:
                return
            }
            tableView.reloadData()
        }
        else {
            
        }
        
    }

    /* Override the editing the table view */
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            /* Custom Alert if anything is deleted from either section */
            let deleteItemAlert = UIAlertController(title: "Delete Item", message: "Are you sure you want to delete this item?", preferredStyle: .alert)
            /* adding the delete button */
            deleteItemAlert.addAction(UIAlertAction(title: "Delete", style: .destructive, handler: { (alert) in
                let header = tableView.headerView(forSection: indexPath.section) as? SectionHeaderViewController
                /* Code to remove the items from the section it originated from and place it in the other section */
                switch indexPath.section {
                case 0:
                    self.list.listItems.remove(at: indexPath.row)
                    let index = IndexPath(item: indexPath.row, section: indexPath.section)
                    tableView.deleteRows(at: [index], with: .left)
                    header?.numberOfListsLabel.text = self.list.listItems.count.description
                case 1:
                    self.list.itemsPurchased.remove(at: indexPath.row)
                    let index = IndexPath(item: indexPath.row, section: indexPath.section)
                    tableView.deleteRows(at: [index], with: .left)
                    header?.numberOfListsLabel.text = self.list.itemsPurchased.count.description
                default:
                    return
                }
                
            }))
            /* adding the cancel button in case there are no changes needed */
            deleteItemAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
    
            self.present(deleteItemAlert, animated: true, completion: nil)
        } else if editingStyle == .insert {
            
        }
    }
    /* Custom alert for when the add button is tapped to add the new item */
    @IBAction func addTapped(_ sender: UIBarButtonItem) {
        
        let addNewItemAlert = UIAlertController(title: "Add New Item", message: "Type the item below", preferredStyle: .alert)
        /* adding text field so the user may enter the new item */
        addNewItemAlert.addTextField { (textField) in
            textField.placeholder = "Enter item name"
        }
        /* Add the add button to the alert */
        addNewItemAlert.addAction(UIAlertAction(title: "Add", style: .default, handler: { (alert) in
            /* adding the new textfield to the alert for user input */
            let textField = addNewItemAlert.textFields![0]
            /* Validate input to make sure there has been something typed */
            if !(textField.text?.isEmpty)!, let newItemName = textField.text {
                /* If the input is valid, add the input to the new item */
                self.list.listItems.append(newItemName)
                /* refresh the table to reflect the new data */
                self.tableView.reloadData()
            }
        }))
        /* cancel button in case there is no changes needed */
        addNewItemAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        /* Show the alert */
        self.present(addNewItemAlert, animated: true, completion: nil)
    }

}
