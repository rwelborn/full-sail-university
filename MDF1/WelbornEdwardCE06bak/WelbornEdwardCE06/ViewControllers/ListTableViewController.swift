//
//  ListTableViewController.swift
//  WelbornEdwardCE06
//
//  Created by Roy Welborn on 8/22/20.
//  Copyright © 2020 Roy Welborn. All rights reserved.
//

import UIKit

class ListTableViewController: UITableViewController {
    
    
    /* Variable Declaration */
    var lists = [ShoppingList]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        /* Method to build the json object */
        BuildJSONObject()
        /* enabling multiple selection */
        tableView.allowsMultipleSelectionDuringEditing = true
        /* registering custom header */
        tableView.register(UINib.init(nibName: "SectionHeader", bundle: nil), forHeaderFooterViewReuseIdentifier: "reuseSectionHeader")
    }
    /* override navigation item to only be the < sign */
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
    /* Build path to local .json file */
    func BuildJSONObject() {
        
        if let path = Bundle.main.path(forResource: "ShoppingList", ofType: ".json") {
            /* Create an URL from the path statement */
            let url = URL(fileURLWithPath: path)
            
            do {
                /* Try to build the data from the json file */
                let data = try Data.init(contentsOf: url)
                let jsonObj = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [Any]
                
                /* Test to see if the json populated correctly */
                print(jsonObj as Any)
                /* Parse the json object */
                ParseJSON(jsonObject: jsonObj)

            }
            catch {
                print(error.localizedDescription)
            }
            /* refresh the tableview to reflect the new data */
            tableView.reloadData()
        }
    }
    
    /* Parse the incoming jsonobject to make the data readable to the tableview */
    func ParseJSON(jsonObject: [Any]?) {
        
        guard let jsonObj = jsonObject else{return}
        
        /* Iterate through JSON array */
        for firstLevelItem in jsonObj {
            guard let object = firstLevelItem as? [String: Any],
                let listData = object["lists"] as? String,
                let listItemData = object["listItems"] as? [String]
                else { continue }
            
            /* Add the new shopping list to the array */
            lists.append(ShoppingList(listName : listData, listItems: listItemData))
        }
        /* Test to see if the list populated correctly */
        print(lists)

    }
    
    // MARK: - Table view data source
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        /* set the number of rows equal to the data count of the list */
        return lists.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        /* Set the custom section header to the sectionheaderviewcontroller xib we created */
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseCell", for: indexPath) as! TableViewCell
        
        /* configuring the cell */
        cell.listNameLabel.text = lists[indexPath.row].listName
        cell.numberOfItemsLabel.text = lists[indexPath.row].numberOfListItems.description
        
        /* return the cell to the tableview */
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        /* set the height for the header */
        return 75
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        /* create the custom header view */
         let header = tableView.dequeueReusableHeaderFooterView(withIdentifier: "reuseSectionHeader") as? SectionHeaderViewController
        /* config the header */
        header?.sectionHeaderLabel.text = "Shopping Lists"
        header?.numberOf_Label.text = "Total Shopping Lists:"
        header?.numberOfListsLabel.text = lists.count.description
        /* return the custom header */
        return header
    }
    
    /* If an edit has been made, commit the transaction */
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        /* When the edit style is .delete, go ahead and delete the row */
        if editingStyle == .delete {
            /* Custom alert to confirm the deletion of the row(s) */
            let deleteSingleList = UIAlertController(title: "Delete List", message: "Are you sure you want to delete the list?", preferredStyle: .alert)
            /* add the delete button to the action */
            deleteSingleList.addAction(UIAlertAction(title: "Delete", style: .destructive, handler: { (alert) in
                // Update the data (delete the list) first
                // MUST delete the object first before deleting the row
                self.lists.remove(at: indexPath.row)
                tableView.deleteRows(at: [indexPath], with: .left)
                // Update the section header view
                let header = self.tableView.headerView(forSection: 0) as? SectionHeaderViewController
                header?.numberOfListsLabel.text = self.lists.count.description
            }))
            // Add cancel button
            deleteSingleList.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
            // Show alert
            self.present(deleteSingleList, animated: true, completion: nil)
        }
            // Cover insertion
        else if editingStyle == .insert {
            
        }
    }
    
    // Fire when multi select row is chosen
    // Catches WillSelectRowAt callback
    override func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        if tableView.isEditing == true {
            print("Selecting Row: " + indexPath.row.description)
        }
        return indexPath
    }
    
    override func tableView(_ tableView: UITableView, willDeselectRowAt indexPath: IndexPath) -> IndexPath? {
        if tableView.isEditing == true {
            print("Unselecting Row: " + indexPath.row.description)
        }
        return indexPath
    }
    
    @IBAction func editTapped(_ sender: UIBarButtonItem) {
        // Enter edit mode, and exit edit mode
        tableView.setEditing(!tableView.isEditing, animated: true)
        
        if tableView.isEditing == true {
            // If edit mode is enabled, set leftBarButton to .trash, and call trash all selected method
            navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .trash, target: self, action: #selector(ListTableViewController.trashAllSelected))
            // IF edit mode is enabled, set rightBarButton to .cancel, and call own method
            navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(ListTableViewController.editTapped(_:)))
        }
        else {
            // If edit mode is disabled then there will be no leftBarButton
            navigationItem.leftBarButtonItem = nil
            // If edit mode is disabled then reset rightBarButton to .edit
            navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .edit, target: self, action: #selector(ListTableViewController.editTapped(_:)))
        }
    }
    
    @IBAction func addTapped(_ sender: UIBarButtonItem) {
        // Create a new list
    }
    
    // Loop through and delete all the comics and cells that have been selected in edit mode
    @objc func trashAllSelected() {
        // Build alert to confirm deletion of selected lists
        let deleteListsAlert = UIAlertController(title: "Delete Lists", message: "Are you sure you want to delete the list(s)?", preferredStyle: .alert)
        // Add delete button
        deleteListsAlert.addAction(UIAlertAction(title: "Delete", style: .destructive, handler: { (alert) in
            // Get index of all selected rows
            if var selectedIPs = self.tableView.indexPathsForSelectedRows {
                // Sort in largest to smallest index, so we can remove items from back to front
                selectedIPs.sort { (a, b) -> Bool in
                    a.row > b.row
                }
                // MARK: need to fix if they select a purchased item
                switch selectedIPs. {
                case <#pattern#>:
                    <#code#>
                default:
                    <#code#>
                }
                for indexPath in selectedIPs {
                    // Update the data (delete list) first
                    self.lists.remove(at: indexPath.row)
                }
                
                // Delete all the rows at once
                self.tableView.deleteRows(at: selectedIPs, with: .left)
                // Update custom section header
                let header = self.tableView.headerView(forSection: 0) as? SectionHeaderViewController
                header?.numberOfListsLabel.text = self.lists.count.description
            }
        }))
        // Add cancel button
        deleteListsAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        // Show alert
        self.present(deleteListsAlert, animated: true, completion: nil)
    }
    
    @IBAction func addNewList(_ sender: UIButton) {
        // Test if custom section header was the initiator
        if (tableView.headerView(forSection: 0) as? SectionHeaderViewController) != nil {
            // Call add new list alert
            addNewListAlert()
        }
    }
    
    func addNewListAlert() {
        // Build alert to create new list
        let addNewListAlert = UIAlertController(title: "Create New List", message: "Please enter the name of the list", preferredStyle: .alert)
        // Add a text field for user input
        addNewListAlert.addTextField { (textField) in
            // Set text field placeholder
            textField.placeholder = "Enter list name"
        }
        // Add a create button
        addNewListAlert.addAction(UIAlertAction(title: "Create", style: .default, handler: { (alert) in
            // Get the text field from the alert itself
            let textField = addNewListAlert.textFields![0]
            // Test if textfield is empty, and if the text is valid String
            if !(textField.text?.isEmpty)!, let newListName = textField.text {
                // Build a new list using the textfield.text as the name
                self.lists.append(ShoppingList(listName: newListName, listItems: []))
                // Reload table data
                self.tableView.reloadData()
            }
        }))
        // Add cancel button
        addNewListAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        // Show alert
        self.present(addNewListAlert, animated: true, completion: nil)
    }
    
    // MARK: - Navigation
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView.isEditing == false {
            // Only allow cell selection to segue if the tableviewcontroller is not in editing mode
            self.performSegue(withIdentifier: "toItemsView", sender: self)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        // When this view is shown again, update the custom section header data
        // And reload the tableview
        let header = self.tableView.headerView(forSection: 0) as? SectionHeaderViewController
        header?.numberOfListsLabel.text = self.lists.count.description
        tableView.reloadData()
    }
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the index of the selected row
        if let indexPath = tableView.indexPathForSelectedRow {
            // Optional bind the grocerylist object
            let groceryListToSend = lists[indexPath.row]
            
            // test segue destination
            if let destination = segue.destination as? ItemsTableViewController {
                // Set destination grocery list object to the optional binded grocery list object
                destination.list = groceryListToSend
            }
        }
    }
}
