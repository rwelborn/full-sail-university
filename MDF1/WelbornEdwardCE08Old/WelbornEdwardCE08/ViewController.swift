//
//  ViewController.swift
//  WelbornEdwardCE04
//
//  Created by Roy Welborn on 6/12/20.
//  Copyright © 2020 Roy Welborn. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    /* Variable Declaration */
    var redditData = [redditGroup]()
    /* Label Declaration */
    @IBOutlet weak var subRedditLabel: UILabel!
    
    /* TableView Declaration */
    @IBOutlet weak var redditTableView: UITableView!
    
    /* Button Declaration */
    @IBAction func settingsButton(_ sender: Any) {
        /* Open settings / theme page */
        
    }
    @IBAction func subRedditButton(_ sender: Any) {
        
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        super.viewDidLoad()
        redditTableView.dataSource = self
        redditTableView.delegate = self
        getRedditJSON(whichReddit: "https://www.reddit.com/r/steampunk_wallpapers/.json")
    }

    func getRedditJSON(whichReddit: String)
    {
        /* Retrieve 1st JSON data from a remote course */
        /* Create a default configuration */
        let firstConfig = URLSessionConfiguration.default
        /* Create a session */
        let firstSession = URLSession(configuration: firstConfig)
        // Validate URL to make sure it is not a broken link
        if let validURL = URL(string: whichReddit) {
            // Create a task that will download whatever is in the validURL as a data object
             let firstTask = firstSession.dataTask(with: validURL) { (data, response, error) in
                  // If there is an error we are going to bail our of this entire method
                  if let error = error {
                      print("Data task failed with error: " + error.localizedDescription)
                      return
                  }
                  // If we get here that means that we have recieved the info at the URL as a data object and we can now use it
                  print("success")
                  
                  // Check the response status, validate the data
                  guard let httpResponse = response as? HTTPURLResponse,
                      httpResponse.statusCode == 200,
                      let validData = data
                  else {
                      print("JSON object creation failed")
                      return
                  }
                print("Returned Data: " + String(validData.count))
                  do {
                    let firstJsonObj = try JSONSerialization.jsonObject(with: validData, options: .mutableContainers) as? [String: Any]
                      
                      // Call the parse method
                    self.parseData(jsonObject: firstJsonObj)
                  }
                  catch {
                      print(error.localizedDescription)
                  }
              }
              firstTask.resume()
        }
    }

    // Parse the jsonObj recieved from the reddit URL
    func parseData(jsonObject: [String: Any]?)
    {
        // Safely Bind jsonObject to the non-optional json
         
         //Loop through second json object
             //Try to convert
        guard let object = jsonObject,
                let dataObject = object["data"] as? [String: Any],
                let childrenObject = dataObject["children"] as? [[String: Any]]
            else { print("Parse Failed to Unwrap jsonObject."); return }
            // print("children object: " + childrenObject.description)
            for items in childrenObject
            {
                guard let subDataObject = items["data"] as? [String: Any]
                    else { continue }
            
                let subRedditData = subDataObject["subreddit"] as? String ?? "No SubReddit"
                let titleData = subDataObject["title"] as? String ?? "No Title"
                let authorNameData = subDataObject["author"] as? String ?? "No Author"
                let thumbNailData = subDataObject["thumbnail"] as? String ?? "No Thumbnail"
//                print(items.description)
                if thumbNailData.contains("https")
                {
                    let newRedditData = redditGroup(subReddit: subRedditData, title: titleData, authorName: authorNameData, imageURLString: thumbNailData)
                    redditData.append(newRedditData)
                }
            }
        print(redditData.count)
        DispatchQueue.main.async {
            self.navigationItem.title = "Subreddit Name:" + self.redditData[0].subReddit.description
            self.redditTableView.reloadData()
        }
 
        print("return Data: " + String(redditData.count))
    }
 
    /* tableview callback protocols */
        
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            /* return the count of the data from the reddit json */
            return redditData.count
        }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            // reuse and existing cell or create a new one if needed
            let cell: UITableViewCell = UITableViewCell(style: UITableViewCell.CellStyle.subtitle, reuseIdentifier: "cellID1")
            cell.textLabel?.text = redditData[indexPath.row].title?.description
            cell.detailTextLabel?.text = redditData[indexPath.row].authorName?.description
            cell.imageView?.image = redditData[indexPath.row].thumbnail
             return cell
        }
}

