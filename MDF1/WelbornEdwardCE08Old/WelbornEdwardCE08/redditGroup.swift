//
//  redditGroup.swift
//  WelbornEdwardCE04
//
//  Created by Roy Welborn on 6/13/20.
//  Copyright © 2020 Roy Welborn. All rights reserved.
//

import Foundation
import UIKit

class redditGroup
{
    /* Stored Properties */
    let subReddit: String
    let title: String?
    let authorName : String?
    var thumbnail: UIImage?
    
    /*Initializers*/
    init(subReddit: String, title: String, authorName: String, imageURLString: String)
    {
        self.subReddit = subReddit
        self.title = title
        self.authorName = authorName
        /* If there is no thumbnail the field will be set to "self", "default", or possibly other non-url strings. Checking against "http" will allow us to tell for sure no matter what the non-image string may be. */
        if imageURLString.contains("http"),
             let url = URL(string: imageURLString),
             var urlComp = URLComponents(url: url, resolvingAgainstBaseURL: false)
         {
             // Change to secure server. All thumbs seem to come from "redditmedia" servers.
             // A simple borswer test proved that redditmedia servers allow secure access.
             // Using URLComponent object to quickly alter the incomming URL from http to https
             // Alternatives include whitelisting a domain with ATS if no secure version is available
             // Or disabling ATS altogether which is HIGHLY undesireable.
             urlComp.scheme = "https"

             if let secureURL = urlComp.url {

                 let imageData = try? Data.init(contentsOf: secureURL)
                 self.thumbnail = UIImage(data: imageData!)

             }
            
         }
 
        /*
        if let url = URL(string: imageURLString)
        {
            do
            {
                let data = try Data(contentsOf: url)
                self.thumbnail = UIImage(data: data)
            }
            catch
            {
                print(error.localizedDescription)
            }
        }
        */
    }
}
