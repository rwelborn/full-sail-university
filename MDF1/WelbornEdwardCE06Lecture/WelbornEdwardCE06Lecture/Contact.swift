//
//  Contact.swift
//  WelbornEdwardCE06Lecture
//
//  Created by Roy Welborn on 6/19/20.
//  Copyright © 2020 Roy Welborn. All rights reserved.
//

import Foundation

class Contact
{
    /* Stored Properties */
    var name: String!
    var relationship: String!
    
    /* Initializer */
    init(name: String, relationship: String)
    {
        self.name = name
        self.relationship = relationship
    }
}
