//
//  TableViewController.swift
//  WelbornEdwardCE06Lecture
//
//  Created by Roy Welborn on 6/18/20.
//  Copyright © 2020 Roy Welborn. All rights reserved.
//

import UIKit

/* Global Private Declaration */
private let g_CellID01 = "CellID01"

class TableViewController: UITableViewController, UISearchBarDelegate, UISearchResultsUpdating, UISearchControllerDelegate {

    /* Variable Declaration */
    var contactArray = [Contact]()
    var filteredArray = [Contact]()

    /* Search Controller Declaration */
    /* Passing nil tells our search controller to use the current tableviewcontroller to display the results */
    var searchController = UISearchController(searchResultsController: nil)
    
    override func viewDidLoad()
    {
        super.viewDidLoad()

        contactArray = [
                   Contact(name: "Bob", relationship: "Uber Driver"),
                   Contact(name: "Jane",   relationship: "Family"),
                   Contact(name: "Ken", relationship: "Friend"),
                   Contact(name: "Ryan",   relationship: "Family"),
                   Contact(name: "Marsha", relationship: "Family"),
                   Contact(name: "Jake",   relationship: "Friend"),
                   Contact(name: "Jim", relationship: "Delivery Guy"),
                   Contact(name: "Greg",   relationship: "Contractor"),
                   Contact(name: "Frank",  relationship: "Friend"),
                   Contact(name: "Charlie",relationship: "Friend"),
                   Contact(name: "Lynn",   relationship: "Family"),
                   Contact(name: "Grace",  relationship: "Family"),
                   Contact(name: "Luke",   relationship: "Friend"),
                   Contact(name: "Merle",  relationship: "Family"),
                   Contact(name: "Sarah",  relationship: "Baby Sitter")
            ]
        filteredArray = contactArray
        /* Setup Search Controller */
        searchController.dimsBackgroundDuringPresentation = false
        searchController.definesPresentationContext = true
        
        /* To recieve update to searches here in this tableviewcontroller */
        searchController.searchResultsUpdater = self
        
        /* Setup search bar of search controller */
        searchController.searchBar.scopeButtonTitles = ["All", "Family", "Friend"]
        searchController.searchBar.delegate = self
        
        /* Add search bar as a naviation item */
        navigationItem.searchController = searchController
        
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int
    {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        // #warning Incomplete implementation, return the number of rows
        return filteredArray.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: g_CellID01, for: indexPath)

        // Configure the cell...
        cell.textLabel?.text = filteredArray[indexPath.row].name
        cell.detailTextLabel?.text = filteredArray[indexPath.row].relationship
        return cell
    }
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    /* Mark: -- Search Callbacks */
    func updateSearchResults(for searchController: UISearchController)
    {
         
        // using the incoming search controller, gather current searchbar text and scope title selected, then filter the results. Needs a sepaerate array to hold the filtered information
        // Get the text the user wants to search for
        let searchText = searchController.searchBar.text
        
        // get the scope button title that the user has selected
        let selectedScope = searchController.searchBar.selectedScopeButtonIndex
        let allScopeTitles = searchController.searchBar.scopeButtonTitles!
        let scopeTitle = allScopeTitles[selectedScope]
        
        // Filter the Data
        // dump full dataset into the arrary we will use to filtering
        filteredArray = contactArray
        
        // If the user entered anything into the search field then filter based on that entry
        if searchText != ""
        {
            /* Below is an inline and un-named fucionted passed into the .filter call as a paramter .
                // .filter is method that takes in another method as a parameter.  Programming
                // 'contact' is the name we've given to our array element that is currently bgeing filtered.
                // Each element in the array will return true if it passes the criteria and will then be
                // included in the filteredArray.
                // Each element in the filteredArray will be then shown in our TableView
             */
            
            filteredArray = filteredArray.filter(
                {
                    (contact) -> Bool in
                    return contact.name.lowercased().range(of: searchText!.lowercased()) != nil
                })
        }
        // filter again based on scope
        if scopeTitle != "All"
        {
            filteredArray = filteredArray.filter(
                {
                    $0.relationship.range(of: scopeTitle) != nil
                })
        }
         tableView.reloadData()
    }
    /* This delegate method is called when the user changes the scope.  Since we handle the in our
      // impemtation of the "updatesearchresults" we can just call that method and let it take
      // care of everything
    */
    func searchBar(_ searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int) {
        updateSearchResults(for: searchController)
    }
}
