//
//  ViewController.swift
//  WelbornEdwardCE03
//
//  Created by Roy Welborn on 09/01/20.
//  Copyright © 2020 Roy Welborn. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITextViewDelegate {

    /* Variable Declarations */
    var counter = 0
    var companyInfo = [Company]()
    var revenueInfo = [Revenue]()
    var colorsInfo = [Colors]()
    var joinedColors = [String]()
    
    /* Label Declarations */
    @IBOutlet weak var companyLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var versionLabel: UILabel!
    @IBOutlet weak var revenueLabel: UILabel!
    @IBOutlet weak var colorsLabel: UILabel!
    @IBOutlet weak var Color1: UILabel!
    @IBOutlet weak var Color2: UILabel!
    @IBOutlet weak var Color3: UILabel!
    @IBOutlet weak var catchPhraseLabel: UILabel!
    
    /* UIObject Button Declarations */
    @IBOutlet weak var previousButton: UIButton!
    @IBOutlet weak var nextButton: UIButton!
    
    /* Text View Declaration */
    @IBOutlet weak var colorsTextView: UITextView!
    
    /* Button Actions */
    @IBAction func previousButton(_ sender: UIButton)
    {
        // Show Previous json object on screen
        counter = counter - 1
        if counter < 0
        {
            counter = 0
        }
        
        colorsTextView.delegate = self
        displayCurrentCompanyIndex(index: counter)
        displayCurrentRevenueIndex(index: counter)
    }
    
    @IBAction func nextButton(_ sender: UIButton)
    {
        // show next json object in on screen
        counter = counter + 1
         if counter >= companyInfo.count
         {
             counter = 0
         }
        colorsTextView.delegate = self
        displayCurrentCompanyIndex(index: counter)
        displayCurrentRevenueIndex(index: counter)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        /* JSON URLS */
        /* https://fullsailmobile.firebaseio.com/T1NVC.json */
        /* https://fullsailmobile.firebaseio.com/T2CRC.json */
        
        /* Button Border Declaractions */
         nextButton.backgroundColor = .clear
         nextButton.layer.cornerRadius = 10
         nextButton.layer.borderWidth = 2
         nextButton.layer.borderColor = UIColor.systemRed.cgColor
         previousButton.backgroundColor = .clear
         previousButton.layer.cornerRadius = 10
         previousButton.layer.borderWidth = 2
         previousButton.layer.borderColor = UIColor.systemRed.cgColor
        
        /* Retrieve 1st JSON data from a remote course */
        /* Create a default configuration */
        let firstConfig = URLSessionConfiguration.default
        /* Create a session */
        let firstSession = URLSession(configuration: firstConfig)
        
        /* Validate URL to make sure it is not a broken link */
        if let validURL1 = URL(string: "https://fullsailmobile.firebaseio.com/T1NVC.json") {
            /* Create a task that will download whatever is in the validURL as a data object */
            let firstTask = firstSession.dataTask(with: validURL1) { (data, response, error) in
                /* If there is an error we are going to bail our of this entire method */
                if let error = error {
                    print("Data task failed with error: " + error.localizedDescription)
                    return
                }
                /* If we get here that means that we have recieved the info at the URL as a data object and we can now use it */
                print("success")
                
                /* Check the response status, validate the data */
                guard let httpResponse = response as? HTTPURLResponse,
                    httpResponse.statusCode == 200,
                    let validData = data
                else {
                    print("JSON object creation failed")
                    return
                }
                do {
                    let firstJsonObj = try JSONSerialization.jsonObject(with: validData, options: .mutableContainers) as? [Any]
                    
                    /* Call the parse method */
                    self.Parse1(jsonObject: firstJsonObj)
                }
                catch {
                    print(error.localizedDescription)
                }
            }
            firstTask.resume()
        }

        /* Retrieve 2nd JSON data from a remote course */
        /* Create a default configuration */
        let secondConfig = URLSessionConfiguration.default
        /* Create a session */
        let secondSession = URLSession(configuration: secondConfig)
        /* Validate URL to make sure it is not a broken link */
        if let validURL2 = URL(string: "https://fullsailmobile.firebaseio.com/T2CRC.json") {
            /* Create a task that will download whatever is in the validURL as a data object */
            let secondTask = secondSession.dataTask(with: validURL2) { (data, response, error) in
                /* If there is an error we are going to bail our of this entire method */
                if let error = error {
                    print("Data task failed with error: " + error.localizedDescription)
                    return
                }
                /* If we get here that means that we have recieved the info at the URL as a data object and we can now use it */
                print("success")
                
                /* Check the response status, validate the data */
                guard let httpResponse = response as? HTTPURLResponse,
                    httpResponse.statusCode == 200,
                    let validData = data
                else {
                    print("JSON object creation failed")
                    return
                }
                do {
                    let secondJsonObj = try JSONSerialization.jsonObject(with: validData, options: .mutableContainers) as? [Any]
                    
                    /* Call the parse method */
                    self.Parse2(jsonObject: secondJsonObj)
                }
                catch {
                    print(error.localizedDescription)
                }
            }
            secondTask.resume()
        }

    }
    func Parse1(jsonObject: [Any]? ) {
        
        /* Safely Bind jsonObject to the non-optional json */
         guard let jsonObj = jsonObject
              else { print("Parse Failed to Unwrap jsonObject."); return }
        
            /* Loop Through items */
            for items in jsonObj
            {
                 guard let object = items as? [String: Any],
                     let companyData = object["company"] as? String,
                     let nameData = object["name"] as? String,
                     let versionData = object["version"] as? String
             else { continue }
                /* Test */
               // print("CompanyInfo: ")
             companyInfo.append(Company(company: companyData, name: nameData, version: versionData))
             }
             print("Company Count: " + String(companyInfo.count))
             displayCurrentCompanyIndex(index: 0)
    }
    
    func Parse2(jsonObject: [Any]? ) {
        
         guard let json = jsonObject
             else { print("Parse Failed to Unwrap jsonObject."); return }
         
         /* Loop through second json object */
         for firstLevelItem in json {
             /* Try to convert */
             guard let object = firstLevelItem as? [String: Any],
                let catchPhraseData = object["catch_phrase"] as? String,
                let colorsData = object["colors"] as? [[String: Any]]
                 /* if not then continue to next iteration */
                 else { continue }
            /* Test */
            // print("CD: " + String(colorsData.count))
            let dailyRevenuData = object["daily_revene"] as? String
            let jsonData =  Revenue(catchPhrase: catchPhraseData, colors: colorsInfo, dailyRevenue: dailyRevenuData ?? "$0.00")
            /* clear colorsInfo for new record */
            colorsInfo.removeAll()
            /* Loop through colors json and add to array */
            for data in colorsData
            {
                guard let colorsString = data["color"] as? String,
                 let description = data["desription"] as? String
                 else { continue }
                /* Test */
                 //  print("CS: " + String(colorsString.count))
                colorsInfo.append(Colors(color: colorsString, description: description))
           }
            /* Test */
            // print("Data: " + String(colorsString.count))
            jsonData.colors = colorsInfo
            revenueInfo.append(jsonData)
            
        }
        print("Revenue Info: " + String(revenueInfo.count))
        displayCurrentRevenueIndex(index: 0)
 
    }
    
    /* Display Cpmapny Data */
    func displayCurrentCompanyIndex(index: Int) {
        
         DispatchQueue.main.async {
            self.companyLabel.text = "Company: \(self.companyInfo[index].company.description)"
            self.nameLabel.text = "User Name: \(self.companyInfo[index].name.description)"
            self.versionLabel.text = "Version:  \(self.companyInfo[index].version.description)"
         }
     }
    
    /* Display Revenue Data, and colors */
    func displayCurrentRevenueIndex(index: Int) {
         
        var colorString: String = ""
        joinedColors.removeAll()
        var colorData: String = ""
        
         DispatchQueue.main.async {
            self.colorsTextView.text = ""
            self.revenueLabel.text = "Daily Revenue: \(self.revenueInfo[index].dailyRevenue.description)"
            self.catchPhraseLabel.text = "Catch Phrase: \(self.revenueInfo[index].catchPhrase.description)"
            
            for i in 0..<self.revenueInfo[index].colors.count
            {
                colorString = self.revenueInfo[index].colors[i].description.description + " color = " +
                  self.revenueInfo[index].colors[i].color.description + "\n"
                self.joinedColors.append(colorString)
            }

            colorData = self.joinedColors.joined()
            /* Test */
 //            print("Color String: " + String(self.revenueInfo[index].colors.count))
            self.colorsTextView.text = colorData
         }
      
     }
}

/* MARK: Previous Feedback */
/*
 Patti DacostaWed Aug 12 @ 07:50 am CDT
 Resubmission accepted.

 JSON Remote Data: Excellent 15/15
 All requirements met.
 JSON Parsing: Excellent 25/25
 All requirements met.
 Model Objects: Excellent 20/20
 All requirements met.
 App Functionality: Excellent 20/20
 All requirements met.
 Code Structure & Efficiency: Excellent 20/20
 All requirements met.
 Roy, please make sure to fix the issue with the image that has been added to the launch screen for future submissions. The image is not actually in the project files and without me removing those references, the lab will not compile. Make sure when adding assets to a project that those assets are copied, this can be double-checked by viewing the project files through a finder window.
 */
