//
//  Company.swift
//  WelbornEdwardCE03
//
//  Created by Roy Welborn on 09/01/20.
//  Copyright © 2020 Roy Welborn. All rights reserved.
//

import Foundation

class Company
{
    let company: String
    let name: String
    let version: String
    
    init(company: String, name: String, version: String){
        self.company = company
        self.name = name
        self.version = version
    }
}
