//
//  Revenue.swift
//  WelbornEdwardCE03
//
//  Created by Roy Welborn on 09/01/20.
//  Copyright © 2020 Roy Welborn. All rights reserved.
//

import Foundation
class Revenue
{
    let catchPhrase: String
    var colors: [Colors]
    let dailyRevenue: String
    
    init(catchPhrase: String, colors: [Colors], dailyRevenue: String) {
        self.catchPhrase = catchPhrase
        self.colors = colors
        self.dailyRevenue = dailyRevenue
    }
}

