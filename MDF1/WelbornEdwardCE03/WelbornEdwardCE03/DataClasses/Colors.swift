//
//  Colors.swift
//  WelbornEdwardCE03
//
//  Created by Roy Welborn on 09/01/20.
//  Copyright © 2020 Roy Welborn. All rights reserved.
//

import Foundation

class Colors
{
    let color: String
    let description: String
    
    init(color: String, description: String) {
        self.color = color
        self.description = description
    }
}
