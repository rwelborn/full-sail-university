//
//  DetailsViewController.swift
//  WelbornEdwardCE05
//
//  Created by Roy Welborn on 6/23/20.
//  Copyright © 2020 Roy Welborn. All rights reserved.
//

import UIKit

class DetailsViewController: UIViewController {

    /* URL Example */
    // https://theunitedstates.io/images/congress/[size]/[bioguide].jpg
    
    /* Variable Declarations */
    var data: Members!
    var party: String = ""
    
    /* Image Declaration */
    @IBOutlet weak var imageView: UIImageView!
    
    
    /* Label Declarations */
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var partyLabel: UILabel!
    @IBOutlet weak var chamberLabel: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        /* Set Title of the navigation item */
        navigationItem.title = "\(data.firstName) \(data.lastName)"
    
        /* Build URL */
        let imageUrlString =  "https://theunitedstates.io/images/congress/225x275/\(data.id).jpg"
        

        /* Grab Image */
        setImage(from: imageUrlString)
        chamberLabel.text = "Chamber: \(data.chamber)"
        titleLabel.text = "Title: \(data.title)"
        switch data.party
        {
        case "R":
            party = "Republican"
        case "D":
            party = "Democrat"
        default:
            party = "Independent"
        }
        partyLabel.text = "Party: \(party) - \(data.state)"
        
        // Do any additional setup after loading the view.
    }
    
   func setImage(from url: String) {
       guard let imageURL = URL(string: url) else { return }
    print(url)
    
           // just not to cause a deadlock in UI!
       DispatchQueue.global().async {
           guard let imageData = try? Data(contentsOf: imageURL) else
           {
            switch self.data.party
             {
             case "R":
                self.imageView.image = UIImage(contentsOfFile:"Republican.jpg")
             case "D":
                 self.imageView.image = UIImage(contentsOfFile:"Democrat.jpg")
             default:
                 self.imageView.image = UIImage(contentsOfFile:"Independent.jpg")
              }
            return
           }

           let image = UIImage(data: imageData)
           DispatchQueue.main.async {
               self.imageView.image = image
           }
       }
   }
}
// MARK: TODO
// all that needs left is the image download
