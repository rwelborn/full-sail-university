//
//  TableViewController.swift
//  WelbornEdwardCE05
//
//  Created by Roy Welborn on 6/17/20.
//  Copyright © 2020 Roy Welborn. All rights reserved.
//

import UIKit

class TableViewController: UITableViewController {
    
    /* Variable Declaration */
    var democrat = [Members]()
    var republican = [Members]()
    var independent = [Members]()
    
    var filteredData = [[Members](), [Members](), [Members]()]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let houseString = "https://api.propublica.org/congress/v1/116/house/members.json"
        let senateString = "https://api.propublica.org/congress/v1/116/senate/members.json"
        
        tableView.rowHeight = 43
        
        getJSON(atURL: houseString)
        getJSON(atURL: senateString)
    }
    
    func getJSON(atURL: String)
    {
        /* Retrieve 1st JSON data from a remote course */
        /* Create a default configuration */
        let firstConfig = URLSessionConfiguration.default
        /* Create a session */
        let firstSession = URLSession(configuration: firstConfig)
        // Validate URL to make sure it is not a broken link
        if let validURL = URL(string: atURL) {
            /* Create the URL request */
            var request = URLRequest(url: validURL)
            /* Set the request header values */
            request.setValue("R4uKjHBm3DcnP4ACk1V1ZcWGh0zoObNRFFlUQc3i", forHTTPHeaderField: "X-API-Key")
            // Create a task that will download whatever is in the validURL as a data object
            let task = firstSession.dataTask(with: request) { (data, response, error) in
                // If there is an error we are going to bail our of this entire method
                if let error = error {
                    print("Data task failed with error: " + error.localizedDescription)
                    return
                }
                // If we get here that means that we have recieved the info at the URL as a data object and we can now use it
                print("success")
                
                // Check the response status, validate the data
                guard let httpResponse = response as? HTTPURLResponse,
                    httpResponse.statusCode == 200,
                    let validData = data
                    else {
                        print("JSON object creation failed")
                        return
                }
                // print(request)
                // print("Returned Data: " + String(validData.count))
                
                do {
                    let jsonObj = try JSONSerialization.jsonObject(with: validData, options: .mutableContainers) as? [String: Any]
                    // print(jsonObj as Any)
                    // Call the parse method
                    
                    self.parseData(jsonObject: jsonObj)
                }
                catch {
                    print(error.localizedDescription)
                }
            }
            task.resume()
        }
    }
    func parseData(jsonObject: [String: Any]?)
    {
        var republicanCount: Int = 0
        var democratCount: Int = 0
        var independentCount: Int = 0
        // print(jsonObject as Any)
        // Safely Bind jsonObject to the non-optional json
        guard let object = jsonObject,
            let resultsObject = object["results"] as? [[String: Any]]
            else { print("Parse Failed to Unwrap jsonObject."); return }
        
        //Loop through second json object
        for results in resultsObject
        {
            guard let chamberObject = results["chamber"] as? String else { print("Parse Failed to Unwrap jsonObject."); return }
            guard let memberObject = results["members"] as? [[String: Any]] else { print("Parse Failed to Unwrap jsonObject."); return }
            for items in memberObject {
                let idData = items["id"] as? String ?? "No ID"
                let firstNameData = items["first_name"] as? String ?? "No First Name"
                let lastNameData = items["last_name"] as? String ?? "No Last Name"
                let partyData = items["party"] as? String ?? "No Party"
                let stateData = items["state"] as? String ?? "No State"
                let titleData = items["title"] as? String ?? "No Title"
                switch partyData
                {
                case "R":
                    
                    republicanCount += 1
                    republican.append(Members(id: idData, chamber: chamberObject, firstName: firstNameData, lastName: lastNameData, title: titleData, state: stateData, party: partyData))
                case "D":
                    democratCount += 1
                    democrat.append(Members(id: idData, chamber: chamberObject, firstName: firstNameData, lastName: lastNameData, title: titleData, state: stateData, party: partyData))
                default:
                    independentCount += 1
                    independent.append(Members(id: idData, chamber: chamberObject, firstName: firstNameData, lastName: lastNameData, title: titleData, state: stateData, party: partyData))
                }
                filteredData[0] = democrat
                filteredData[1] = republican
                filteredData[2] = independent
            }
            
        }
        
        //Try to convert
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
        
       // print("Counts D: \(democratCount.description) R: \(republicanCount.description) I: \(independentCount.description)")
    }
    
    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 3
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return filteredData[section].count
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String?
    {
        switch section {
        case 0:
            return "Democrats"
        case 1:
            return "Republican"
        case 2:
            return "Independent"
        default:
            return "Ooops, Should not be here"
        }
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "CellID01", for: indexPath) as? CongressTableViewCell else { return UITableViewCell() }
        
        switch indexPath.section {
        case 0:
            cell.contentView.backgroundColor = UIColor.blue
            cell.nameLabel.textColor = UIColor.white
            cell.chamberLabel.textColor = UIColor.white
            cell.partyLabel.textColor = UIColor.white
        case 1:
            cell.contentView.backgroundColor = UIColor.red
            cell.nameLabel.textColor = UIColor.white
            cell.chamberLabel.textColor = UIColor.white
            cell.partyLabel.textColor = UIColor.white
        case 2:
            cell.contentView.backgroundColor = UIColor.yellow
            cell.nameLabel.textColor = UIColor.black
            cell.chamberLabel.textColor = UIColor.black
            cell.partyLabel.textColor = UIColor.black
        default:
            cell.contentView.backgroundColor = UIColor.white
        }
        
        let currentPost = filteredData[indexPath.section][indexPath.row]
        
        cell.nameLabel.text = "\(currentPost.firstName) \(currentPost.lastName)"
        cell.chamberLabel.text = currentPost.title
        cell.partyLabel.text = " \(currentPost.party) - \(currentPost.state)"
        return cell
        
    }
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if let indexPath = tableView.indexPathForSelectedRow
        {
            let dataToSend = filteredData[indexPath.section][indexPath.row]
            
            if let destination = segue.destination as? DetailsViewController
            {
                destination.data = dataToSend
            }
        }
        
    }
    func filterPostsByParty() {
        
        filteredData[0] = republican.filter({ $0.party == "R" })
        filteredData[1] = democrat.filter({ $0.party == "D" })
        filteredData[2] = independent.filter({ $0.party == "I" })
    }
}
/* MARK: Previous Feedback */
/* I cleared the warnings and commented out the print statements as well */
/* Feedback from previous assignment */
/*
 Awesome!

 JSON Remote Data | Parsing: Excellent 15/15
 All requirements met.
 UITableView: Excellent 25/25
 All requirements met.
 Detail View: Excellent 25/25
 All requirements met.
 Model Objects: Excellent 15/15
 All requirements met.
 Code Structure & Efficiency: Excellent 20/20
 Make sure to resolve warnings prior to submission, in this case the offending warning is actually a print statement that could have been commented out or removed.
 Roy, much, much better! This is very solid work and meets all of the rubric requirements earning a top grade, great job!
 */
