//
//  CongressTableViewCell.swift
//  WelbornEdwardCE05
//
//  Created by Roy Welborn on 6/23/20.
//  Copyright © 2020 Roy Welborn. All rights reserved.
//

import UIKit

class CongressTableViewCell: UITableViewCell {

    /* Label Declarations */
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var chamberLabel: UILabel!
    @IBOutlet weak var partyLabel: UILabel!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
