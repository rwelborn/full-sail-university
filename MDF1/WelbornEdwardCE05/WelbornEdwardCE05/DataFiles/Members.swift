//
//  Members.swift
//  WelbornEdwardCE05
//
//  Created by Roy Welborn on 6/22/20.
//  Copyright © 2020 Roy Welborn. All rights reserved.
//

import Foundation
import UIKit

class Members
{
    /* Stored Properites */
    let id: String
    let chamber: String
    let firstName: String
    let lastName: String
    let title: String
    let state: String
    let party: String
    
    /* Initializers */
    init(id: String, chamber: String, firstName: String, lastName: String, title: String, state: String, party: String)
    {
        self.id = id
        self.chamber = chamber
        self.firstName = firstName
        self.lastName = lastName
        self.state = state
        self.title = title
        self.party = party
    }
}
