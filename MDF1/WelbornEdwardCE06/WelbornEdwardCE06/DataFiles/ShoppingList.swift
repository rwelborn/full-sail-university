//
//  ShoppingList.swift
//  WelbornEdwardCE06
//
//  Created by Roy Welborn on 8/22/20.
//  Copyright © 2020 Roy Welborn. All rights reserved.
//

import Foundation
class ShoppingList {
    
    /* Stored Properties */
    var listName: String = ""
    var listItems = [String]()
    var itemsPurchased = [String]()
    
    /* Computed Properties */
    var numberOfListItems: Int {
        return listItems.count
    }
    
   /* initializers (trying something new I saw) */
    init(listName: String, listItems: [String] = [String]()) {
        self.listName = listName
        self.listItems = listItems
    }
}
