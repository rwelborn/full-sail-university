//
//  ListTableViewController.swift
//  WelbornEdwardCE06
//
//  Created by Roy Welborn on 8/22/20.
//  Copyright © 2020 Roy Welborn. All rights reserved.
//
import UIKit

class TableViewCell: UITableViewCell {
    
    /* Label Declaractions */
    @IBOutlet weak var listNameLabel: UILabel!
    @IBOutlet weak var numberOfItemsLabel: UILabel!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    /* configure the view for the selected state */
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
