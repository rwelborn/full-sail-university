//
//  ItemsTableViewController.swift
//  WelbornEdwardCE06
//
//  Created by Roy Welborn on 8/22/20.
//  Copyright © 2020 Roy Welborn. All rights reserved.
//

import UIKit

class ItemsTableViewController: UITableViewController {

     
    /* Variable Declarations */
    var itemList: ShoppingList!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        /* enabling multiple selection */
        tableView.allowsMultipleSelectionDuringEditing = true
        /* Set the title to the name of each list */
        navigationItem.title = "\(itemList.listName) List Details"
        /* Add the righthand Bar Button to the navigationBar, adds a button calls editTapped method */
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .edit, target: self, action: #selector(ItemsTableViewController.editTapped(_:)))
        /* Registers the section header xib to the itemstableviewcontroller */
        tableView.register(UINib(nibName: "SectionHeader", bundle: nil), forHeaderFooterViewReuseIdentifier: "reuseSectionHeader")
        tableView.allowsSelectionDuringEditing = true
    }
    
    /* Method that handles the edit button when pressed */
    @IBAction func editTapped(_ sender: UIButton) {
         /* Enters and exits edit mode depending on the button press */
         tableView.setEditing(!tableView.isEditing, animated: true)
         
         if tableView.isEditing == true {
             /* when edit mode is enabled, this will set the leftBarButton to .trash (trashcan icon), and calls the trash all method for the items selected */
             navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .trash, target: self, action: #selector(ItemsTableViewController.trashSelected))
              /* When edit mode is enabled, this will set the rightBarButton to .cancel */
             navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(ItemsTableViewController.editTapped(_:)))
         }
         else {
             /* When edit mode is disabled the leftBarButton will not be shown */
             navigationItem.leftBarButtonItem = nil
             /* When edit mode is disabled the rightBarButton to the default .edit */
             navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .edit, target: self, action: #selector(ItemsTableViewController.editTapped(_:)))
         }
     }
    /* delete all the items and cells that have been selected */
    @objc func trashSelected() {
        /* build the custom alert for deleting items from the list */
        let deleteRowsAlert = UIAlertController(title: "Delete Lists", message: "Are you sure you want to delete the list(s)?", preferredStyle: .alert)
        /* add the delete button to the alert */
        deleteRowsAlert.addAction(UIAlertAction(title: "Delete", style: .destructive, handler: { (alert) in
            
            /* Grab the index of selected items */
            if var selectedRows = self.tableView.indexPathsForSelectedRows {
                /* Sort the rows so we can delete the row from end to beginning */
                selectedRows.sort { (a, b) -> Bool in
                    a.row > b.row
                }
                for indexPath in selectedRows {
                    /* update the list(s) */
                    switch indexPath.section {
                    case 0:
                        self.itemList.listItems.remove(at: indexPath.row)
                    case 1:
                        self.itemList.itemsPurchased.remove(at: indexPath.row)
                    default:
                        print("Forbidden Zone, we should never be here")
                    }
                }
                
                /* Delete all the selected rows */
                self.tableView.deleteRows(at: selectedRows, with: .left)
                // Update custom section header
                let header = self.tableView.headerView(forSection: 0) as? SectionHeaderViewController
                header?.numberOfListsLabel.text = self.itemList.listItems.count.description
            }
            self.tableView.setEditing(false, animated: true)
            /* turn off edit mode, and reset the buttons */
            self.navigationItem.leftBarButtonItem = nil
             /* change the button back to edit */
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .edit, target: self, action: #selector(ItemsTableViewController.editTapped(_:)))
        }))
        /* add the cancel button to the alert */
        deleteRowsAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        /* show the custom alert */
        self.present(deleteRowsAlert, animated: true, completion: nil)
 
    }
    
    @IBAction func addNewList(_ sender: UIButton) {
        /* Make sure the header is correct */
        if (tableView.headerView(forSection: 0) as? SectionHeaderViewController) != nil {
            /* add new items to the header */
            addToTheListAlert()
        }
    }
    
    func addToTheListAlert() {
         /*  build custom alert to create a new list */
         let addItemAlert = UIAlertController(title: "Add a New Item", message: "Please enter the name of the Item", preferredStyle: .alert)
         /* add textfield for user input */
         addItemAlert.addTextField { (textField) in
             /* set a place holder example for the user */
             textField.placeholder = "Enter Item Name"
         }
         /* add the create button */
         addItemAlert.addAction(UIAlertAction(title: "Add Item", style: .default, handler: { (alert) in
             /*  read the user input */
             let textField = addItemAlert.textFields![0]
             /* // verify that the input from the text field is valid and not empy */
             if !(textField.text?.isEmpty)!, let newListItem = textField.text {
                 /* build the new list from the text field input */
                self.itemList.listItems.append(newListItem)
                 /* reload the table data */
                 self.tableView.reloadData()
             }
            self.tableView.setEditing(false, animated: true)
             /* disable left button  */
             self.navigationItem.leftBarButtonItem = nil
              /* set the right button back to edit */
             self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .edit, target: self, action: #selector(ItemsTableViewController.editTapped(_:)))
         }))
         /* add cancel button */
         addItemAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
         /* show the alert */
         self.present(addItemAlert, animated: true, completion: nil)

     }
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        /* Configure  sections in tableview for items needed and items purchased */
        return 2
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        /*  switch method to show the count of each list item or purchased item */
        var rowCount = 0
        switch section {
        case 0:
            rowCount += itemList.listItems.count
        case 1:
            rowCount += itemList.itemsPurchased.count
        default:
            rowCount = 0
        }
        print("Number of Rows:  \(rowCount)")
        return rowCount
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)

        /* Configuring the cell(s) */
        switch indexPath.section {
        case 0:
            cell.textLabel?.text = itemList.listItems[indexPath.row]
        case 1:
            cell.textLabel?.text = itemList.itemsPurchased[indexPath.row]
        default:
            cell.textLabel?.text = ""
        }

        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        /* set the custom header height */
        return 75
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        /* Set the custom section header to the sectionheaderviewcontroller xib we created */
        let itemsHeader = tableView.dequeueReusableHeaderFooterView(withIdentifier: "reuseSectionHeader") as? SectionHeaderViewController
        
        /* Custom Header button declaration */
        itemsHeader?.headerButton.isHidden = false
        itemsHeader?.headerButton.addTarget(self, action: #selector(addTapped), for: .touchUpInside)
        itemsHeader?.numberOf_Label.text = "Number of Items:"
        /* Update the header labels for each section by which list it is */
        switch section {
        case 0:
            itemsHeader?.sectionHeaderLabel.text = "Items Needed"
            itemsHeader?.numberOfListsLabel.text = itemList.listItems.count.description
        case 1:
            itemsHeader?.sectionHeaderLabel.text = "Items Purchased"
            itemsHeader?.numberOfListsLabel.text = itemList.itemsPurchased.count.description
        default:
            itemsHeader?.sectionHeaderLabel.text = ""
            itemsHeader?.numberOf_Label.text = ""
            itemsHeader?.numberOfListsLabel.text = ""
        }
        
        return itemsHeader
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        /* Using a switch statement to find the cell selection in each section, add it to the opposite section, then remove it from its initial section */
        if tableView.isEditing != true {
            switch indexPath.section {
            case 0:
                itemList.itemsPurchased.append(itemList.listItems[indexPath.row])
                itemList.listItems.remove(at: indexPath.row)
            case 1:
                itemList.listItems.append(itemList.itemsPurchased[indexPath.row])
                itemList.itemsPurchased.remove(at: indexPath.row)
            default:
                return
            }
            /* reload the tableView */
            tableView.reloadData()
        }
        else {
            
        }
        
    }

    /* Override the editing the table view */
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            /* Custom Alert if anything is deleted from either section */
            let deleteItemAlert = UIAlertController(title: "Delete Item", message: "Are you sure you want to delete this item?", preferredStyle: .alert)
            /* adding the delete button */
            deleteItemAlert.addAction(UIAlertAction(title: "Delete", style: .destructive, handler: { (alert) in
                let header = tableView.headerView(forSection: indexPath.section) as? SectionHeaderViewController
                /* Code to remove the items from the section it originated from and place it in the other section */
                switch indexPath.section {
                case 0:
                    self.itemList.listItems.remove(at: indexPath.row)
                    let index = IndexPath(item: indexPath.row, section: indexPath.section)
                    tableView.deleteRows(at: [index], with: .left)
                    header?.numberOfListsLabel.text = self.itemList.listItems.count.description
                case 1:
                    self.itemList.itemsPurchased.remove(at: indexPath.row)
                    let index = IndexPath(item: indexPath.row, section: indexPath.section)
                    tableView.deleteRows(at: [index], with: .left)
                    header?.numberOfListsLabel.text = self.itemList.itemsPurchased.count.description
                default:
                    return
                }
                
            }))
            /* adding the cancel button in case there are no changes needed */
            deleteItemAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
    
            self.present(deleteItemAlert, animated: true, completion: nil)
        } else if editingStyle == .insert {
            
        }
    }
    /* Custom alert for when the add button is tapped to add the new item */
    @IBAction func addTapped(_ sender: UIBarButtonItem) {
        
        let addNewItemAlert = UIAlertController(title: "Add New Item", message: "Type the item below", preferredStyle: .alert)
        /* adding text field so the user may enter the new item */
        addNewItemAlert.addTextField { (textField) in
            textField.placeholder = "Enter item name"
        }
        /* Add the add button to the alert */
        addNewItemAlert.addAction(UIAlertAction(title: "Add", style: .default, handler: { (alert) in
            /* adding the new textfield to the alert for user input */
            let textField = addNewItemAlert.textFields![0]
            /* Validate input to make sure there has been something typed */
            if !(textField.text?.isEmpty)!, let newItemName = textField.text {
                /* If the input is valid, add the input to the new item */
                self.itemList.listItems.append(newItemName)
                /* refresh the table to reflect the new data */
                self.tableView.reloadData()
            }
        }))
        /* cancel button in case there is no changes needed */
        addNewItemAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        /* Show the alert */
        self.present(addNewItemAlert, animated: true, completion: nil)
    }

}
