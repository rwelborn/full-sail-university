//
//  ListTableViewController.swift
//  WelbornEdwardCE06
//
//  Created by Roy Welborn on 8/22/20.
//  Copyright © 2020 Roy Welborn. All rights reserved.
//

import UIKit

class SectionHeaderViewController: UITableViewHeaderFooterView {
    
    /* label Declarations */
    @IBOutlet weak var sectionHeaderLabel: UILabel!
    @IBOutlet weak var numberOf_Label: UILabel!
    @IBOutlet weak var numberOfListsLabel: UILabel!
    @IBOutlet weak var headerButton: UIButton!
    
}
