//
//  ListTableViewController.swift
//  WelbornEdwardCE06
//
//  Created by Roy Welborn on 8/22/20.
//  Copyright © 2020 Roy Welborn. All rights reserved.
//

import UIKit

class ListTableViewController: UITableViewController {
    
    
    /* Variable Declaration */
    var itemList = [ShoppingList]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        /* Method to build the json object */
        BuildJSONObject()
        /* enabling multiple selection */
        tableView.allowsMultipleSelectionDuringEditing = true
        /* registering custom header */
        tableView.register(UINib.init(nibName: "SectionHeader", bundle: nil), forHeaderFooterViewReuseIdentifier: "reuseSectionHeader")
    }
    /* override navigation item to only be the < sign */
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
    /* Build path to local .json file */
    func BuildJSONObject() {
        
        if let path = Bundle.main.path(forResource: "ShoppingList", ofType: ".json") {
            /* Create an URL from the path statement */
            let url = URL(fileURLWithPath: path)
            
            do {
                /* Try to build the data from the json file */
                let data = try Data.init(contentsOf: url)
                let jsonObj = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [Any]
                
                /* Test to see if the json populated correctly */
                // print(jsonObj as Any)
                /* Parse the json object */
                ParseJSON(jsonObject: jsonObj)

            }
            catch {
                print(error.localizedDescription)
            }
            /* refresh the tableview to reflect the new data */
            tableView.reloadData()
        }
    }
    
    /* Parse the incoming jsonobject to make the data readable to the tableview */
    func ParseJSON(jsonObject: [Any]?) {
        
        guard let jsonObj = jsonObject else{return}
        
        /* Iterate through JSON array */
        for firstLevelItem in jsonObj {
            guard let object = firstLevelItem as? [String: Any],
                let listData = object["lists"] as? String,
                let listItemData = object["listItems"] as? [String]
                else { continue }
            
            /* Add the new shopping list to the array */
            itemList.append(ShoppingList(listName : listData, listItems: listItemData))
        }
        /* Test to see if the list populated correctly */
        // print(lists)

    }
    
    // MARK: - Table view data source
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        /* set the number of rows equal to the data count of the list */
        return itemList.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        /* Set the custom section header to the sectionheaderviewcontroller xib we created */
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseCell", for: indexPath) as! TableViewCell
        
        /* configuring the cell */
        cell.listNameLabel.text = itemList[indexPath.row].listName
        cell.numberOfItemsLabel.text = itemList[indexPath.row].numberOfListItems.description
        
        /* return the cell to the tableview */
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        /* set the height for the header */
        return 75
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        /* create the custom header view */
         let header = tableView.dequeueReusableHeaderFooterView(withIdentifier: "reuseSectionHeader") as? SectionHeaderViewController
        /* config the header */
        header?.sectionHeaderLabel.text = "Shopping Lists"
        header?.numberOf_Label.text = "Total Shopping Lists:"
        header?.numberOfListsLabel.text = itemList.count.description
        /* return the custom header */
        return header
    }
    
    /* If an edit has been made, commit the transaction */
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        /* When the edit style is .delete, go ahead and delete the row */
        if editingStyle == .delete {
            /* Custom alert to confirm the deletion of the row(s) */
            let deleteSingleList = UIAlertController(title: "Delete List", message: "Are you sure you want to delete the list?", preferredStyle: .alert)
            /* add the delete button to the action */
            deleteSingleList.addAction(UIAlertAction(title: "Delete", style: .destructive, handler: { (alert) in
                /* update the list */
                self.itemList.remove(at: indexPath.row)
                tableView.deleteRows(at: [indexPath], with: .left)
                // Update the section header view
                let header = self.tableView.headerView(forSection: 0) as? SectionHeaderViewController
                header?.numberOfListsLabel.text = self.itemList.count.description
            }))
            /* add cancel button */
            deleteSingleList.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
            /* show the alert */
            self.present(deleteSingleList, animated: true, completion: nil)
        }
        else if editingStyle == .insert {
            
        }
    }
    
    /* Handles Multi Selection */
    override func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        if tableView.isEditing == true {
            print("Row Selected: " + indexPath.row.description)
        }
        return indexPath
    }
    
    override func tableView(_ tableView: UITableView, willDeselectRowAt indexPath: IndexPath) -> IndexPath? {
        if tableView.isEditing == true {
            print("Row Deselected: " + indexPath.row.description)
        }
        return indexPath
    }
    
    @IBAction func editTapped(_ sender: UIBarButtonItem) {
        /* enter and exit edit mode */
        tableView.setEditing(!tableView.isEditing, animated: true)
        
        if tableView.isEditing == true {
            /* If edit mode is enabled, set leftBarButton to the trash icon, and call trashselected function */
            navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .trash, target: self, action: #selector(ListTableViewController.trashAllSelected))
            /* If edit mode is enabled, set rightBarButton to the cancel icon */
            navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(ListTableViewController.editTapped(_:)))
        }
        else {
            /* If edit mode is disabled then there will be no button on the left */
            navigationItem.leftBarButtonItem = nil
            /* If edit mode is disabled then change the right button to edit */
            navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .edit, target: self, action: #selector(ListTableViewController.editTapped(_:)))
        }
    }
    
    @IBAction func addTapped(_ sender: UIBarButtonItem) {
        /* function to create a new list */
    }
    
    /* itterate through and delete the selectioned lists and cells that have been selected in edit mode */
    @objc func trashAllSelected() {
        /* build a custom alert and confirm the deletion of any selected rows */
        let deleteListsAlert = UIAlertController(title: "Delete Lists", message: "Are you sure you want to delete the list(s)?", preferredStyle: .alert)
        /* add a delete button to the alert */
        deleteListsAlert.addAction(UIAlertAction(title: "Delete", style: .destructive, handler: { (alert) in
            /* gather index of all selected rows */
            if var selectedIPs = self.tableView.indexPathsForSelectedRows {
                /* sort the rows so we can delete from last to first */
                selectedIPs.sort { (a, b) -> Bool in
                    a.row > b.row
                }
                for indexPath in selectedIPs {
                    /* update the data and remove the rows */
                    self.itemList.remove(at: indexPath.row)
                }
                
                /* delete the rows in the table */
                self.tableView.deleteRows(at: selectedIPs, with: .left)
                /* update the header */
                let header = self.tableView.headerView(forSection: 0) as? SectionHeaderViewController
                header?.numberOfListsLabel.text = self.itemList.count.description
            }
            self.tableView.setEditing(false, animated: true)
             /* if edit is disabled, hide the lefthand button */
            self.navigationItem.leftBarButtonItem = nil
             /* If edit mode is disabled then the rightBarButton is set to .edit */
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .edit, target: self, action: #selector(ListTableViewController.editTapped(_:)))
        }))
        /* add cance button to the alert */
        deleteListsAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        /* show the alert */
        self.present(deleteListsAlert, animated: true, completion: nil)
 
    }
    
    @IBAction func addNewList(_ sender: UIButton) {
        if (tableView.headerView(forSection: 0) as? SectionHeaderViewController) != nil {
            /* Call add new list alert */
            addNewListAlert()
        }
    }
    
    func addNewListAlert() {
        /* builds custom alert for adding a new list */
        let addNewListAlert = UIAlertController(title: "Create New List", message: "Please enter the name of the list", preferredStyle: .alert)
        /* adds a text field to get user inpus */
        addNewListAlert.addTextField { (textField) in
            /* set texfield placeholder as an example to the user */
            textField.placeholder = "Enter list name"
        }
        /* add a cancel button to the custom alert */
        addNewListAlert.addAction(UIAlertAction(title: "Create", style: .default, handler: { (alert) in
            /* get user input from the textfield */
            let textField = addNewListAlert.textFields![0]
            /* ensure we have valid input and the textfield is not empty */
            if !(textField.text?.isEmpty)!, let newListName = textField.text {
                /* add the new list using the user input as the list name */
                self.itemList.append(ShoppingList(listName: newListName, listItems: []))
                /* reload the tableView */
                self.tableView.reloadData()
            }
            self.tableView.setEditing(false, animated: true)
            /* remove leftbutton if not in edit mode */
            self.navigationItem.leftBarButtonItem = nil
            /* if not in edit mode the right hand button needs to be set to edit */
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .edit, target: self, action: #selector(ListTableViewController.editTapped(_:)))
        }))
        /* add cancel button to the custom alert */
        addNewListAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        /* show the custom alert */
        self.present(addNewListAlert, animated: true, completion: nil)

    }
    
    // MARK: - Navigation
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView.isEditing == false {
            /* only allow selection when the table is not in edit mode */
            self.performSegue(withIdentifier: "toItemsView", sender: self)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        /* update the custom header */
        let header = self.tableView.headerView(forSection: 0) as? SectionHeaderViewController
        header?.numberOfListsLabel.text = self.itemList.count.description
        /* refresh the tableView */
        tableView.reloadData()
    }
    
    /* prepare statement for navigation */
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        /* grab the index for the selected row */
        if let indexPath = tableView.indexPathForSelectedRow {
            
            let shoppingListToSend = itemList[indexPath.row]
            
            /* make sure the segue destination is valid */
            if let destination = segue.destination as? ItemsTableViewController {
                destination.itemList = shoppingListToSend
            }
        }
    }
}
