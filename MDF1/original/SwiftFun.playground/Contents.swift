// If Statements

let height = 59

if height >= 48 {
    print("You can ride the ride!")
} else {
    print("Drink your milk and come back next year kid...")
}

// If 80 or hotter "It's hot!!"
// If less than 80, "It's cold"

var temp = 41

if temp >= 80 {
    print("It's hot!!")
} else {
    print("It's cold")
}

