# Full Sail University Repository!

This repository will be used to keep track of my _FullSail Univeristy Program Assignments_ 
This repository well hold all code that is written during my tenure at Full Sail Univerisity.  This repository may also contain code fragments of code that may be used in future programming projects.

## DVP3 Code and Assignments

This zip hold my project and porfolio 3 assignments and code

Code Exercise 1

In this exercise we created a program with custom even handlers as a pre-requisite to the final project

Code Exersise 2

In this code exercise we created and connected to a database

Code Exercise 3

This exercise was a precursor to the final project, we connected to the API we chose for our final project, and made a proof that we could connect, and parse the data that came back to the application

Final Project

This was the final project for our API.  I chose the star wars API (swapi.co).  I think it turned out pretty well I plan on making this into a mobile application and perhaps upload it to the google play store and apple app store


## APL (Apple Programming Language)

This folder holds documents and source code for my APL(Apple Programming Language) course.

## Statistics

This zip file contains my coursework assignments for my statistics course which will be archived shortly



